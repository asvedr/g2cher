package proto

import (
	e "entities/common"
	"time"
)

type IPostRepo interface {
	Register(...*e.NewDbPost) error
	SetNotified(time.Time, ...string) error
	SetSeen(time.Time, ...string) error
	GetById(...string) (map[string]*e.DbPost, error)
	GetPostsForNotify(min_last_seen time.Time) ([]*e.DbPost, error)
	GetNotSeenForLong(threshold time.Time) ([]string, error)
	Remove(...string) error
}

type IResourceRepo interface {
	Add(e.Resource) error
	Update(e.UpdResource) error
	GetAll() ([]*e.Resource, error)
	Remove(names ...string) error
}

type IPredicateRepo interface {
	GetMap(resources ...string) (map[string][]*e.PredicateData, error)
	Add(e.NewPredicateData) (int, error)
	Remove(ids ...int) error
}
