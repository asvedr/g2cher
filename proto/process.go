package proto

import "time"

type IProcess interface {
	Name() string
	Run() error
}

type IStepProcess interface {
	Name() string
	RunIteration() error
	SleepTime() time.Duration
}

type step_proc struct{ proc IStepProcess }

func (self step_proc) Name() string {
	return self.proc.Name()
}

func (self step_proc) Run() error {
	proc := self.proc
	for {
		err := proc.RunIteration()
		if err != nil {
			return err
		}
		time.Sleep(proc.SleepTime())
	}
}

func StepProcToProc(proc IStepProcess) IProcess {
	return step_proc{proc: proc}
}
