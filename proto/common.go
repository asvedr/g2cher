package proto

import (
	e "entities/common"

	"golang.org/x/net/html"
)

type IPostParser interface {
	Name() string
	ParsePage(node *html.Node) ([]*e.NewDbPost, error)
}

type IPostFetcher interface {
	Fetch(url string, parser IPostParser) ([]*e.NewDbPost, error)
}

type IPredicate interface {
	String() string
	SetData(*e.PredicateData)
	Data() *e.PredicateData
	Check(*e.DbPost) bool
}

type IPredicateCompiler interface {
	Compile(src string) (IPredicate, error)
}

type IResourceManager interface {
	GetAll() []e.Resource
	GetResource(name string) (e.Resource, bool)
	UpdResource(e.Resource) error
	AddResource(e.Resource) error
	RemoveResources(res_name []string) error
	GetPredicates(res_name string) []IPredicate
	AddPredicate(res_name string, pred_name string, rule string, include bool) error
	RemovePredicate(res_name string, pred_name string) error
}

// Send msg to telegram
type INotifier interface {
	Notify(msg string) error
}

type IUiCmd interface {
	Name() string
	Help() string
	Process(msg string) (string, error)
}
