package run

import (
	"log"
	"proto"

	"gitlab.com/asvedr/cli_run"
	"gitlab.com/asvedr/giter"
)

type UseCase struct {
	cli_run.BaseEntrypoint
	proc []proto.IProcess
}

func New(proc ...proto.IProcess) cli_run.IEntrypoint {
	return &UseCase{proc: proc}
}

func maker(proc proto.IProcess) error {
	err := proc.Run()
	if err != nil {
		log.Printf("process %s failed on: %v", proc.Name(), err)
	}
	return err
}

func (UseCase) Command() string {
	return "run"
}

func (UseCase) Description() string {
	return "Run fetcher + notifier"
}

func (self UseCase) Execute(prog string, request any) error {
	return giter.GatherMapErr(maker, self.proc...)
}
