package run

import (
	"time"

	"gitlab.com/asvedr/cli_run"
)

type parser struct {
	generic cli_run.IGenericParser
}

type request struct {
	DbPath  string
	Timeout time.Duration
}

func new_parser() cli_run.IArgParser {
	p := cli_run.GenericParser()
	p.AddDefaultParam(
		[]string{"-d"},
		cli_run.Str{Default: "g2cher.db"},
		"db path",
	)
	var zero int64 = 0
	p.AddDefaultParam(
		[]string{"-t"},
		cli_run.Int{
			Gt:      &zero,
			Default: 10,
		},
		"timeout between requests(in seconds)",
	)
	return &parser{generic: p}
}

func (p parser) Parse(args []string) (any, error) {
	parsed, err := p.generic.Parse(args)
	if err != nil {
		return nil, err
	}
	seconds := parsed.Named["-t"].(int64)
	req := request{
		DbPath:  parsed.Named["-d"].(string),
		Timeout: time.Second * time.Duration(seconds),
	}
	return req, nil
}

func (p parser) Params() []string {
	return p.generic.IntoArgParser().Params()
}
