package config_schema

import (
	"config"

	"gitlab.com/asvedr/cli_run"
)

type UseCase struct {
	cli_run.BaseEntrypoint
}

func New() cli_run.IEntrypoint {
	return &UseCase{}
}

func (UseCase) Command() string {
	return "config-schema"
}

func (UseCase) Description() string {
	return "Show config fields"
}

func (UseCase) Execute(prog string, request any) error {
	for _, field := range config.GetFields() {
		println(field)
	}
	return nil
}
