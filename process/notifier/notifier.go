package notifier

import (
	ents "entities/common"
	"fmt"
	"log"
	"proto"
	"time"

	"gitlab.com/asvedr/giter"
)

type notifier struct {
	notifier    proto.INotifier
	post_repo   proto.IPostRepo
	res_manager proto.IResourceManager
	timeout     time.Duration
	max_age     time.Duration
}

func New(
	n proto.INotifier,
	post_repo proto.IPostRepo,
	res_manager proto.IResourceManager,
	timeout time.Duration,
	max_age time.Duration,
) proto.IStepProcess {
	return &notifier{
		notifier:    n,
		post_repo:   post_repo,
		res_manager: res_manager,
		timeout:     timeout,
		max_age:     max_age,
	}
}

func (notifier) Name() string {
	return "notifier"
}

func (notifier) SleepTime() time.Duration {
	return time.Second
}

func (self *notifier) RunIteration() error {
	ma := -self.max_age
	now := time.Now()
	posts, err := self.post_repo.GetPostsForNotify(now.Add(ma))
	if err != nil {
		return err
	}
	f := func(post *ents.DbPost) {
		err := self.notify_about_post(post)
		if err != nil {
			log.Printf("Notify post %s error: %v", post.Id, err)
		}
	}
	giter.GatherMap(f, posts...)
	return nil
}

func (self notifier) notify_about_post(post *ents.DbPost) error {
	preds := self.res_manager.GetPredicates(post.ResourceName)
	check_status, check_msg := check_preds(preds, post)
	if !check_status {
		log.Printf("post %s skipped: %s", post.Id, check_msg)
		log.Printf("notified at: %v", post.Notified)
		return self.post_repo.SetNotified(time.Now(), post.Id)
	}
	now := time.Now()
	msg := fmt.Sprintf(
		"%s\n%s\nlast_seen: %v(%s ago)\nnotified: %v(%s ago)",
		make_prefix(post),
		post.Link,
		post.LastSeenOnBoard,
		now.Sub(post.LastSeenOnBoard),
		post.Notified,
		now.Sub(post.Notified),
	)
	if err := self.notifier.Notify(msg); err != nil {
		return err
	}
	return self.post_repo.SetNotified(time.Now(), post.Id)
}

func check_preds(preds []proto.IPredicate, post *ents.DbPost) (bool, string) {
	var msg string
	for _, pred := range preds {
		if pred.Data().Mode == ents.PredicateModeInclude {
			if !pred.Check(post) {
				msg = fmt.Sprintf("inc(%s) = false", pred.Data().Name)
				return false, msg
			}
		} else {
			if pred.Check(post) {
				msg = fmt.Sprintf("exc(%s) = true", pred.Data().Name)
				return false, msg
			}
		}
	}
	if len(preds) == 0 {
		msg = "len(preds) = 0"
		return false, msg
	}
	return true, msg
}

func make_prefix(post *ents.DbPost) string {
	if post.Notified.Unix() == 0 {
		return "New post"
	} else {
		return "Post still active"
	}
}
