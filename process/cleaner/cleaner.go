package cleaner

import (
	"log"
	"proto"
	"time"
)

type cleaner struct {
	post_repo     proto.IPostRepo
	max_life_time time.Duration
}

func New(
	post_repo proto.IPostRepo,
	max_life_time time.Duration,
) proto.IStepProcess {
	return &cleaner{
		post_repo:     post_repo,
		max_life_time: max_life_time,
	}
}

func (cleaner) Name() string {
	return "cleaner"
}

func (cleaner) SleepTime() time.Duration {
	return time.Second * time.Duration(10)
}

func (self *cleaner) RunIteration() error {
	tm := -self.max_life_time
	now := time.Now()
	ids, err := self.post_repo.GetNotSeenForLong(now.Add(tm))
	if err != nil {
		return err
	}
	log.Printf("Remove old posts: %s\n", ids)
	err = self.post_repo.Remove(ids...)
	if err != nil {
		return err
	}
	return nil
}
