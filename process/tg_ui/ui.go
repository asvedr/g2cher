package tg_ui

import (
	"fmt"
	"log"
	"net/http"
	"proto"
	"strings"
	"time"

	"github.com/PaulSonOfLars/gotgbot/v2"
	"github.com/PaulSonOfLars/gotgbot/v2/ext"
	"github.com/PaulSonOfLars/gotgbot/v2/ext/handlers"
	"github.com/PaulSonOfLars/gotgbot/v2/ext/handlers/filters/message"
	"gitlab.com/asvedr/giter"
)

type notifier struct {
	commands  []proto.IUiCmd
	post_repo proto.IPostRepo
	// pred_repo proto.IPredicateRepo
	compiler proto.IPredicateCompiler
	token    string
	admin_id int64
}

func New(
	commands []proto.IUiCmd,
	post_repo proto.IPostRepo,
	compiler proto.IPredicateCompiler,
	token string,
	admin_id int64,
) proto.IProcess {
	return &notifier{
		commands:  commands,
		post_repo: post_repo,
		// pred_repo: pred_repo,
		compiler: compiler,
		token:    token,
		admin_id: admin_id,
	}
}

func (notifier) Name() string {
	return "tg_notifier"
}

func (self notifier) Run() error {
	bot, err := gotgbot.NewBot(self.token, &gotgbot.BotOpts{
		BotClient: &gotgbot.BaseBotClient{
			Client: http.Client{},
			DefaultRequestOpts: &gotgbot.RequestOpts{
				Timeout: gotgbot.DefaultTimeout, // Customise the default request timeout here
				APIURL:  gotgbot.DefaultAPIURL,  // As well as the Default API URL here (in case of using local bot API servers)
			},
		},
	})
	if err != nil {
		return fmt.Errorf("failed to create new bot: %v", err)
	}
	// Create updater and dispatcher.
	updater := ext.NewUpdater(&ext.UpdaterOpts{
		Dispatcher: ext.NewDispatcher(&ext.DispatcherOpts{
			// If an error is returned by a handler, log it and continue going.
			Error: func(b *gotgbot.Bot, ctx *ext.Context, err error) ext.DispatcherAction {
				log.Println("an error occurred while handling update:", err.Error())
				return ext.DispatcherActionNoop
			},
			MaxRoutines: ext.DefaultMaxRoutines,
		}),
	})
	dispatcher := updater.Dispatcher

	handler := func(bot *gotgbot.Bot, ctx *ext.Context) error {
		if ctx.EffectiveMessage.From.Id == self.admin_id {
			return self.react_admin(bot, ctx)
		} else {
			return echo(bot, ctx)
		}
	}

	// Add echo handler to reply to all text messages.
	dispatcher.AddHandler(
		handlers.NewMessage(message.Text, handler),
	)

	// Start receiving updates.
	err = updater.StartPolling(bot, &ext.PollingOpts{
		DropPendingUpdates: true,
		GetUpdatesOpts: &gotgbot.GetUpdatesOpts{
			Timeout: 9,
			RequestOpts: &gotgbot.RequestOpts{
				Timeout: time.Second * 10,
			},
		},
	})
	if err != nil {
		panic("failed to start polling: " + err.Error())
	}
	log.Printf("%s has been started...\n", bot.User.Username)

	// Idle, to keep updates coming in, and avoid bot stopping.
	updater.Idle()
	return nil
}

func (self notifier) react_admin(b *gotgbot.Bot, ctx *ext.Context) error {
	msg := ctx.EffectiveMessage
	var err error
	if msg.Text == "/help" {
		_, err = msg.Reply(b, self.prepare_help(), nil)
		return nil
	}
	for _, cmd := range self.commands {
		if strings.HasPrefix(msg.Text, cmd.Name()) {
			response, err := cmd.Process(msg.Text)
			if err != nil {
				response = "Error: " + err.Error()
			}
			_, err = msg.Reply(b, response, nil)
			return err
		}
	}
	_, err = ctx.EffectiveMessage.Reply(b, "Command not found", nil)
	return err
}

func (self notifier) prepare_help() string {
	f := func(c proto.IUiCmd) string {
		return c.Name() + " " + c.Help()
	}
	return strings.Join(giter.Map(self.commands, f), "\n")
}

// echo replies to a messages with its own contents.
func echo(b *gotgbot.Bot, ctx *ext.Context) error {
	id := ctx.EffectiveMessage.From.Id
	name := ctx.EffectiveMessage.From.Username
	_, err := ctx.EffectiveMessage.Reply(
		b,
		fmt.Sprintf("Hello %v: %s", id, name),
		nil,
	)
	if err != nil {
		return fmt.Errorf("failed to echo message: %w", err)
	}
	return nil
}
