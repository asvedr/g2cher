package fetcher

import (
	ents "entities/common"
	errs "entities/errors"
	"fmt"
	"log"
	"proto"
	"time"

	"gitlab.com/asvedr/giter"
)

type fetcher struct {
	fetcher     proto.IPostFetcher
	parsers     map[string]proto.IPostParser
	res_manager proto.IResourceManager
	post_repo   proto.IPostRepo
	timeout     time.Duration
}

type separated struct {
	new_posts []*ents.NewDbPost
	old_posts []*ents.NewDbPost
}

func New(
	f proto.IPostFetcher,
	parsers map[string]proto.IPostParser,
	res_manager proto.IResourceManager,
	post_repo proto.IPostRepo,
	timeout time.Duration,
) proto.IStepProcess {
	return &fetcher{
		fetcher:     f,
		parsers:     parsers,
		res_manager: res_manager,
		post_repo:   post_repo,
		timeout:     timeout,
	}
}

func (fetcher) Name() string {
	return "fetcher"
}

func (self *fetcher) SleepTime() time.Duration {
	return self.timeout
}

func (self *fetcher) RunIteration() error {
	maker := func(r ents.Resource) error {
		return self.process_resource(r)
	}
	err := giter.GatherMapErr(maker, self.res_manager.GetAll()...)
	if err != nil {
		return err
	}
	return nil
}

func (self fetcher) process_resource(resource ents.Resource) error {
	if self.is_resource_inactive(resource) {
		return nil
	}
	parser, found := self.parsers[resource.Parser]
	if !found {
		log.Printf("Parser %s not found", resource.Parser)
	}
	posts, err := self.fetcher.Fetch(resource.Path, parser)
	if err != nil {
		switch err.(type) {
		case *errs.ErrRequestError:
		case *errs.ErrInvalidResp:
		case *errs.ErrCanNotParsePage:
			log.Printf("Can not fetch posts: %v", err)
		default:
			return err
		}
	}
	log.Printf("fetched %d posts(resource=%s)", len(posts), resource.Name)
	return self.process_posts(resource, posts)
}

func (self fetcher) is_resource_inactive(resource ents.Resource) bool {
	if !resource.Active {
		log.Printf("Resource(%s) disabled. Skip.", resource.Name)
		return true
	}
	if len(self.res_manager.GetPredicates(resource.Name)) == 0 {
		log.Printf("Resource(%s) has no predicates. Skip.", resource.Name)
		return true
	}
	return false
}

func (self fetcher) process_posts(
	resource ents.Resource,
	posts []*ents.NewDbPost,
) error {
	posts = self.filter_posts(resource, posts)
	if len(posts) == 0 {
		log.Printf("all posts was filtered")
	}
	for _, post := range posts {
		post.ResourceName = resource.Name
	}
	separated, err := self.separate_by_new_or_not(posts)
	if err != nil {
		return err
	}
	log.Printf(
		"posts after filters: %d (new=%d)",
		len(posts),
		len(separated.new_posts),
	)
	if err = self.post_repo.Register(separated.new_posts...); err != nil {
		return err
	}
	if err = self.update_last_seen(separated.old_posts); err != nil {
		return err
	}
	return err
}

func (self fetcher) update_last_seen(posts []*ents.NewDbPost) error {
	if len(posts) == 0 {
		return nil
	}
	f := func(p *ents.NewDbPost) string { return p.Id }
	ids := giter.Map(posts, f)
	return self.post_repo.SetSeen(time.Now(), ids...)
}

func (self fetcher) filter_posts(
	resource ents.Resource,
	posts []*ents.NewDbPost,
) []*ents.NewDbPost {
	preds := self.res_manager.GetPredicates(resource.Name)
	f := func(p *ents.NewDbPost) bool {
		return is_post_good(preds, p)
	}
	return giter.Filter(posts, f)
}

func (self fetcher) separate_by_new_or_not(posts []*ents.NewDbPost) (separated, error) {
	id_list := giter.Map(posts, func(p *ents.NewDbPost) string { return p.Id })
	in_base_posts, err := self.post_repo.GetById(id_list...)
	if err != nil {
		return separated{}, err
	}
	var in, out []*ents.NewDbPost
	for _, post := range posts {
		if _, found := in_base_posts[post.Id]; found {
			in = append(in, post)
		} else {
			out = append(out, post)
		}
	}
	return separated{new_posts: out, old_posts: in}, nil
}

func is_post_good(predicates []proto.IPredicate, post *ents.NewDbPost) bool {
	include_found := false
	db_post := ents.DbPost{NewDbPost: *post}
	for _, predicate := range predicates {
		if !predicate.Check(&db_post) {
			continue
		}
		switch predicate.Data().Mode {
		case ents.PredicateModeInclude:
			include_found = true
		case ents.PredicateModeExclude:
			return false
		default:
			panic(fmt.Sprintf("Invalid predicate mode: %v", predicate.Data().Mode))
		}
	}
	return include_found
}
