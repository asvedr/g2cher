package cli_notifier

type Notifier struct{}

func (Notifier) Notify(msg string) error {
	println("NOTIFIER: " + msg)
	return nil
}
