package sqlite

import (
	"database/sql"
	ents "entities/common"
	errs "entities/errors"
	"fmt"
	"proto"
	"strings"

	"gitlab.com/asvedr/giter"
	ss "gitlab.com/asvedr/safe_sqlite"
)

type ResourceRepo struct {
	base ss.Db
}

const q_cr_res_table = `
CREATE TABLE IF NOT EXISTS g2c_res (
	Name string primary key not null,
	Path string not null,
	Parser string not null,
	Active boolean not null,
	NotifInterval integer not null
)
`

func NewResourceRepo(base ss.Db) (proto.IResourceRepo, error) {
	err := base.Exec(q_cr_res_table)
	repo := ResourceRepo{base: base}
	return &repo, err
}

func (self *ResourceRepo) Add(res ents.Resource) error {
	if res.NotifInterval == 0 {
		panic("notif interval = 0")
	}
	err := self.base.Exec(
		`INSERT INTO g2c_res
		(Name,Path,Parser,Active,NotifInterval)
		VALUES (?,?,?,?,?)`,
		res.Name,
		res.Path,
		res.Parser,
		res.Active,
		res.NotifInterval,
	)
	if err == nil {
		return nil
	}
	if strings.Contains(err.Error(), "UNIQUE") {
		return &errs.ErrResourceNameAlreadyUsed{}
	}
	return err
}

func (self *ResourceRepo) Update(upd ents.UpdResource) error {
	fields, params := dump_update(upd)
	if len(fields) == 0 {
		return nil
	}
	query := fmt.Sprintf(
		"UPDATE g2c_res SET %s WHERE Name = ?",
		strings.Join(fields, ","),
	)
	params = append(params, upd.Name)
	return self.base.Exec(query, params...)
}

func (self *ResourceRepo) GetAll() ([]*ents.Resource, error) {
	deser := func(row *sql.Rows) (*ents.Resource, error) {
		var Name, Path, Parser string
		var Active bool
		var NotifInterval int
		err := row.Scan(&Name, &Path, &Parser, &Active, &NotifInterval)
		if err != nil {
			return nil, err
		}
		res := &ents.Resource{
			Name:          Name,
			Path:          Path,
			Parser:        Parser,
			Active:        Active,
			NotifInterval: NotifInterval,
		}
		return res, nil
	}
	return ss.FetchSlice(
		self.base,
		deser,
		`SELECT Name,Path,Parser,Active,NotifInterval
		FROM g2c_res ORDER BY Name`,
	)
}

func (self *ResourceRepo) Remove(names ...string) error {
	if len(names) == 0 {
		return nil
	}
	query := fmt.Sprintf(
		"DELETE FROM g2c_res WHERE Name IN (?%s)",
		strings.Repeat(",?", len(names)-1),
	)
	params := giter.Map(names, func(id string) any { return id })
	return self.base.Exec(query, params...)
}

func dump_update(upd ents.UpdResource) ([]string, []any) {
	var fields []string
	var params []any
	if upd.Path != nil {
		fields = append(fields, "Path = ?")
		params = append(params, *upd.Path)
	}
	if upd.Parser != nil {
		fields = append(fields, "Parser = ?")
		params = append(params, *upd.Parser)
	}
	if upd.Active != nil {
		fields = append(fields, "Active = ?")
		params = append(params, *upd.Active)
	}
	if upd.NotifInterval != nil {
		fields = append(fields, "NotifInterval = ?")
		params = append(params, *upd.NotifInterval)
	}
	return fields, params
}
