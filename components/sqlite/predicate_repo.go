package sqlite

import (
	"database/sql"
	ents "entities/common"
	errs "entities/errors"
	"fmt"
	"proto"
	"strings"
	"time"

	"gitlab.com/asvedr/giter"
	ss "gitlab.com/asvedr/safe_sqlite"
)

type PredicateRepo struct{ base ss.Db }

const q_cr_pred_table string = `
CREATE TABLE IF NOT EXISTS g2c_pred (
	Id integer primary key not null,
	Name string not null,
	Data string not null,
	Mode int not null,
	ResourceName string not null,
	Created integer not null,
	FOREIGN KEY(ResourceName) REFERENCES g2c_res(Name)
	UNIQUE(ResourceName,Name)
)`

func NewPredicateRepo(base ss.Db) (proto.IPredicateRepo, error) {
	if err := base.Exec(q_cr_pred_table); err != nil {
		return nil, err
	}
	return &PredicateRepo{base: base}, nil
}

func (self *PredicateRepo) GetMap(resources ...string) (map[string][]*ents.PredicateData, error) {
	query := "SELECT Id,Name,Data,Mode,ResourceName FROM g2c_pred ORDER BY Id"
	deser := func(rows *sql.Rows) (*ents.PredicateData, error) {
		var pd ents.PredicateData
		var mode int
		err := rows.Scan(&pd.Id, &pd.Name, &pd.Data, &mode, &pd.ResourceName)
		pd.Mode = ents.PredicateMode(mode)
		return &pd, err
	}
	rows, err := ss.FetchSlice(self.base, deser, query)
	if err != nil {
		return nil, err
	}
	f := func(p *ents.PredicateData) string { return p.ResourceName }
	return giter.Group(rows, f), nil
}

func (self *PredicateRepo) Add(pred ents.NewPredicateData) (int, error) {
	query := "INSERT INTO g2c_pred(Name, Data, Mode, ResourceName, Created) VALUES (?, ?, ?, ?, ?)"
	id, err := self.base.ExecReturnId(
		query,
		pred.Name,
		pred.Data,
		int(pred.Mode),
		pred.ResourceName,
		time.Now().Unix(),
	)
	if err == nil {
		return int(id), nil
	}
	if strings.Contains(err.Error(), "UNIQUE") {
		return 0, &errs.ErrPredicateNameAlreadyUsed{}
	}
	return 0, err
}

func (self *PredicateRepo) Remove(ids ...int) error {
	if len(ids) == 0 {
		return nil
	}
	query := fmt.Sprintf(
		"DELETE FROM g2c_pred WHERE Id IN (?%s)",
		strings.Repeat(",?", len(ids)-1),
	)
	params := giter.Map(ids, func(id int) any { return id })
	return self.base.Exec(query, params...)
}
