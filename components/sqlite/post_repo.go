package sqlite

import (
	"database/sql"
	ents "entities/common"
	"fmt"
	"proto"
	"strings"
	"time"

	"gitlab.com/asvedr/giter"
	ss "gitlab.com/asvedr/safe_sqlite"
)

type PostRepo struct {
	base ss.Db
}

const q_cr_post_table string = `
CREATE TABLE IF NOT EXISTS g2c_post (
	Id        string primary key not null,
	Link      string not null,
	Label     string not null,
	Responses int not null,
	Notified  int not null,
	LastSeenOnBoard int not null,
	ResourceName string not null,
	FOREIGN KEY(ResourceName) REFERENCES g2c_res(Name)
)`

func NewPostRepo(base ss.Db) (proto.IPostRepo, error) {
	err := base.Exec(q_cr_post_table)
	repo := PostRepo{base: base}
	return &repo, err
}

func (self PostRepo) Register(posts ...*ents.NewDbPost) error {
	if len(posts) == 0 {
		return nil
	}
	tmpl := "(?, ?, ?, ?, ?, ?, ?)"
	var params []any
	now := time.Now().Unix()
	for _, post := range posts {
		if len(post.Id) == 0 || len(post.ResourceName) == 0 {
			panic(post.Id + "|" + post.ResourceName)
		}
		params = append(
			params,
			post.Id,
			post.Link,
			post.Label,
			post.Responses,
			post.ResourceName,
			now,
			0,
		)
	}
	query := fmt.Sprintf(
		`INSERT INTO g2c_post 
		(Id, Link, Label, Responses, ResourceName, LastSeenOnBoard, Notified)
		VALUES %s%s`,
		tmpl,
		strings.Repeat(","+tmpl, len(posts)-1),
	)
	return self.base.Exec(query, params...)
}

func (self PostRepo) SetNotified(when time.Time, id_list ...string) error {
	if len(id_list) == 0 {
		return nil
	}
	params := []any{when.Unix()}
	for _, id := range id_list {
		params = append(params, id)
	}
	query := fmt.Sprintf(
		"UPDATE g2c_post SET Notified = ? WHERE Id IN (?%s)",
		strings.Repeat(",?", len(id_list)-1),
	)
	return self.base.Exec(query, params...)
}

func (self PostRepo) SetSeen(when time.Time, id_list ...string) error {
	if len(id_list) == 0 {
		return nil
	}
	params := []any{when.Unix()}
	for _, id := range id_list {
		params = append(params, id)
	}
	query := fmt.Sprintf(
		"UPDATE g2c_post SET LastSeenOnBoard = ? WHERE Id IN (?%s)",
		strings.Repeat(",?", len(id_list)-1),
	)
	return self.base.Exec(query, params...)
}

func (self PostRepo) GetById(ids ...string) (map[string]*ents.DbPost, error) {
	if len(ids) == 0 {
		return map[string]*ents.DbPost{}, nil
	}
	query := fmt.Sprintf(
		"SELECT %s FROM g2c_post WHERE Id IN (?%s)",
		post_fields,
		strings.Repeat(",?", len(ids)-1),
	)
	params := giter.Map(ids, func(s string) any { return s })
	fetched, err := ss.FetchSlice(self.base, deser_post, query, params...)
	if err != nil {
		return nil, err
	}
	f := func(x *ents.DbPost) (string, *ents.DbPost) {
		return x.Id, x
	}
	return giter.V2M(fetched, f), nil
}

func (self PostRepo) GetPostsForNotify(min_last_seen time.Time) ([]*ents.DbPost, error) {
	query := fmt.Sprintf(
		`
		SELECT %s
		FROM g2c_post as p JOIN g2c_res as r
		ON p.ResourceName = r.Name
		WHERE p.Notified + r.NotifInterval <= %d
			AND LastSeenOnBoard >= %d
		`,
		post_fields,
		time.Now().Unix(),
		min_last_seen.Unix(),
	)
	return ss.FetchSlice(self.base, deser_post, query)
}

func (self PostRepo) GetNotSeenForLong(threshold time.Time) ([]string, error) {
	return ss.FetchSlice(
		self.base,
		func(r *sql.Rows) (string, error) {
			var id string
			err := r.Scan(&id)
			return id, err
		},
		"SELECT Id FROM g2c_post WHERE LastSeenOnBoard < ?",
		threshold.Unix(),
	)
}

func (self PostRepo) Remove(id_list ...string) error {
	if len(id_list) == 0 {
		return nil
	}
	query := fmt.Sprintf(
		"DELETE FROM g2c_post WHERE Id IN (?%s)",
		strings.Repeat(",?", len(id_list)-1),
	)
	params := giter.Map(id_list, func(s string) any { return s })
	return self.base.Exec(query, params...)
}

const post_fields string = "Id,Link,Label,Responses,Notified,LastSeenOnBoard,ResourceName"

func deser_post(rows *sql.Rows) (*ents.DbPost, error) {
	var notif, ls int64
	var p ents.DbPost
	err := rows.Scan(
		&p.Id,
		&p.Link,
		&p.Label,
		&p.Responses,
		&notif,
		&ls,
		&p.ResourceName,
	)
	if err != nil {
		return nil, err
	}
	p.Notified = time.Unix(notif, 0)
	p.LastSeenOnBoard = time.Unix(ls, 0)
	return &p, nil
}
