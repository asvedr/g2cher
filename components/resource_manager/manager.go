package resource_manager

import (
	ents "entities/common"
	errs "entities/errors"
	"fmt"
	"proto"
	"sync"

	"gitlab.com/asvedr/giter"
)

type cache_node struct {
	resource *ents.Resource
	preds    []proto.IPredicate
}

type Manager struct {
	res_repo  proto.IResourceRepo
	pred_repo proto.IPredicateRepo
	compiler  proto.IPredicateCompiler

	cache map[string]*cache_node
	mtx   sync.Mutex
}

func New(
	res_repo proto.IResourceRepo,
	pred_repo proto.IPredicateRepo,
	compiler proto.IPredicateCompiler,
) (proto.IResourceManager, error) {
	man := &Manager{
		res_repo:  res_repo,
		pred_repo: pred_repo,
		compiler:  compiler,

		cache: make(map[string]*cache_node),
	}
	return man, man.fetch_from_db()
}

func (self *Manager) fetch_from_db() error {
	resources, err := self.res_repo.GetAll()
	if err != nil {
		return fmt.Errorf("can not fetch res: %v", err)
	}
	self.cache = giter.V2M(
		resources,
		func(r *ents.Resource) (string, *cache_node) {
			return r.Name, &cache_node{resource: r}
		},
	)
	mapped, err := self.pred_repo.GetMap(giter.MKeys(self.cache)...)
	if err != nil {
		return fmt.Errorf("can not fetch pred: %v", err)
	}
	compile := func(pd *ents.PredicateData) (proto.IPredicate, error) {
		p, err := self.compiler.Compile(pd.Data)
		if err != nil {
			return nil, err
		}
		p.SetData(pd)
		return p, nil
	}
	for res_name, preds_src := range mapped {
		preds, err := giter.MapErr(preds_src, compile)
		if err != nil {
			return fmt.Errorf("can not compile: %v", err)
		}
		cache := self.cache[res_name]
		cache.preds = preds
		self.cache[res_name] = cache
	}
	return nil
}

func (self *Manager) GetAll() []ents.Resource {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	return giter.M2V(
		self.cache,
		func(_ string, c *cache_node) ents.Resource { return *c.resource },
	)
}

func (self *Manager) GetResource(name string) (ents.Resource, bool) {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	val, found := self.cache[name]
	if !found {
		return ents.Resource{}, false
	}
	return *val.resource, true
}

func (self *Manager) UpdResource(res ents.Resource) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	cached, found := self.cache[res.Name]
	if !found {
		return &errs.ErrResourceNameNotFound{}
	}
	upd := cached.resource.ToUpdate(res)
	err := self.res_repo.Update(upd)
	if err != nil {
		return err
	}
	cached.resource = &res
	return nil
}

func (self *Manager) GetPredicates(res_name string) []proto.IPredicate {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	val, found := self.cache[res_name]
	if !found {
		return []proto.IPredicate{}
	}
	return val.preds
}

func (self *Manager) AddResource(res ents.Resource) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	err := self.res_repo.Add(res)
	if err != nil {
		return err
	}
	self.cache[res.Name] = &cache_node{
		resource: &res,
		preds:    []proto.IPredicate{},
	}
	return nil
}

func (self *Manager) RemoveResources(res_names []string) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	var pred_ids []int
	for _, res_name := range res_names {
		cache, found := self.cache[res_name]
		if !found {
			continue
		}
		for _, pred := range cache.preds {
			pred_ids = append(pred_ids, pred.Data().Id)
		}
	}
	err := self.pred_repo.Remove(pred_ids...)
	if err != nil {
		return err
	}
	err = self.res_repo.Remove(res_names...)
	if err != nil {
		return err
	}
	for _, name := range res_names {
		delete(self.cache, name)
	}
	return nil
}

func (self *Manager) AddPredicate(res_name string, pred_name string, rule string, include bool) error {
	compiled, err := self.compiler.Compile(rule)
	if err != nil {
		return err
	}
	var mode ents.PredicateMode
	if include {
		mode = ents.PredicateModeInclude
	} else {
		mode = ents.PredicateModeExclude
	}
	pd := ents.PredicateData{}
	pd.Name = pred_name
	pd.Data = rule
	pd.Mode = mode
	pd.ResourceName = res_name
	self.mtx.Lock()
	defer self.mtx.Unlock()
	cache, found := self.cache[res_name]
	if !found {
		return &errs.ErrResourceNameNotFound{}
	}
	id, err := self.pred_repo.Add(pd.Downcast())
	if err != nil {
		return err
	}
	pd.Id = id
	compiled.SetData(&pd)
	cache.preds = append(cache.preds, compiled)
	return nil
}

func (self *Manager) RemovePredicate(res_name string, pred_name string) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	cache, found := self.cache[res_name]
	if !found {
		return &errs.ErrResourceNameNotFound{}
	}
	index := giter.Find(
		cache.preds,
		func(p proto.IPredicate) bool {
			return p.Data().Name == pred_name
		},
	)
	if index == -1 {
		return nil
	}
	err := self.pred_repo.Remove(cache.preds[index].Data().Id)
	if err != nil {
		return err
	}
	cache.preds = append(cache.preds[:index], cache.preds[index+1:]...)
	return nil
}
