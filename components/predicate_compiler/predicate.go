package predicate_compiler

import (
	ents "entities/common"

	"gitlab.com/asvedr/gexpr"
)

type Predicate struct {
	node gexpr.INode
	data *ents.PredicateData
	fmap map[string]fun_spec
}

func (self *Predicate) String() string {
	return self.node.Raw()
}

func (self *Predicate) Data() *ents.PredicateData {
	return self.data
}

func (self *Predicate) SetData(data *ents.PredicateData) {
	self.data = data
}

func (self *Predicate) Check(post *ents.DbPost) bool {
	ctx := ctx{fmap: self.fmap, post: post}
	return exec_op(ctx, self.node)
}
