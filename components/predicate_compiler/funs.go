package predicate_compiler

import (
	ents "entities/common"
	"strings"
	"time"

	"gitlab.com/asvedr/gexpr"
)

type ctx struct {
	fmap map[string]fun_spec
	post *ents.DbPost
}

type fun_spec struct {
	f    func(ctx, []gexpr.INode) bool
	args []gexpr.NodeType
}

func new_fun_spec(f func(ctx, []gexpr.INode) bool, args ...gexpr.NodeType) fun_spec {
	return fun_spec{f: f, args: args}
}

func f_and(c ctx, args []gexpr.INode) bool {
	return exec_op(c, args[0]) && exec_op(c, args[1])
}

func f_or(c ctx, args []gexpr.INode) bool {
	return exec_op(c, args[0]) || exec_op(c, args[1])
}

func f_not(c ctx, args []gexpr.INode) bool {
	return !exec_op(c, args[0])
}

func f_gt(c ctx, args []gexpr.INode) bool {
	return c.get_int(args[0]) > c.get_int(args[1])
}

func f_gte(c ctx, args []gexpr.INode) bool {
	return c.get_int(args[0]) >= c.get_int(args[1])
}

func f_lt(c ctx, args []gexpr.INode) bool {
	return c.get_int(args[0]) < c.get_int(args[1])
}

func f_lte(c ctx, args []gexpr.INode) bool {
	return c.get_int(args[0]) <= c.get_int(args[1])
}

func f_eq(c ctx, args []gexpr.INode) bool {
	return c.get_int(args[0]) == c.get_int(args[1])
}

func f_in(c ctx, args []gexpr.INode) bool {
	tmpl := strings.ToLower(c.get_str(args[0]))
	label := strings.ToLower(c.get_str(args[1]))
	return strings.Contains(label, tmpl)
}

func f_notified(c ctx, args []gexpr.INode) bool {
	threshold := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	return c.post.Notified.Unix() > threshold.Unix()
}

func (self ctx) get_int(node gexpr.INode) int {
	switch node.Type() {
	case gexpr.NodeTypeInt:
		return int(node.Int())
	case gexpr.NodeTypeVar:
		switch node.VarName() {
		case var_resp:
			return self.post.Responses
		default:
			panic("Unexpected var: " + node.VarName())
		}
	default:
		panic("Unexpected node type: " + node.Type().String())
	}
}

func (self ctx) get_str(node gexpr.INode) string {
	switch node.Type() {
	case gexpr.NodeTypeStr:
		return node.StringVal()
	case gexpr.NodeTypeVar:
		switch node.VarName() {
		case var_label:
			return self.post.Label
		default:
			panic("Unexpected var: " + node.VarName())
		}
	default:
		panic("Unexpected node type: " + node.Type().String())
	}
}
