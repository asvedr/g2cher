package predicate_compiler

import (
	errs "entities/errors"
	"errors"
	"fmt"
	"proto"

	"gitlab.com/asvedr/gexpr"
)

type compiler struct {
	parser gexpr.IParser
	fmap   map[string]fun_spec
}

func NewCompiler() proto.IPredicateCompiler {
	parser := gexpr.NewParser(gexpr.ParserConfig{
		BinOperators: map[string]int{
			">":  1,
			"<":  1,
			"<=": 1,
			">=": 1,
			"==": 1,
			"in": 1,
			"&":  0,
			"|":  0,
		},
		UnOperators: []string{"!"},
		Consts:      []string{var_resp, var_label},
		Funcs:       []string{"notified"},
		DenyReal:    true,
	})
	fmap := map[string]fun_spec{
		"&":        new_fun_spec(f_and, gexpr.NodeTypeCall, gexpr.NodeTypeCall),
		"|":        new_fun_spec(f_or, gexpr.NodeTypeCall, gexpr.NodeTypeCall),
		"!":        new_fun_spec(f_not, gexpr.NodeTypeCall),
		">":        new_fun_spec(f_gt, gexpr.NodeTypeInt, gexpr.NodeTypeInt),
		"<":        new_fun_spec(f_lt, gexpr.NodeTypeInt, gexpr.NodeTypeInt),
		">=":       new_fun_spec(f_gte, gexpr.NodeTypeInt, gexpr.NodeTypeInt),
		"<=":       new_fun_spec(f_lte, gexpr.NodeTypeInt, gexpr.NodeTypeInt),
		"==":       new_fun_spec(f_eq, gexpr.NodeTypeInt, gexpr.NodeTypeInt),
		"in":       new_fun_spec(f_in, gexpr.NodeTypeStr, gexpr.NodeTypeStr),
		"notified": new_fun_spec(f_notified),
	}
	return &compiler{parser: parser, fmap: fmap}
}

func (self *compiler) Compile(src string) (proto.IPredicate, error) {
	node, err := self.parser.Parse(src)
	if err != nil {
		return nil, &errs.ErrPredicateBuilderFailed{Src: src, Cause: err}
	}
	err = self.validate_call(src, self.fmap, node)
	return &Predicate{node: node, fmap: self.fmap}, err
}

func (self *compiler) validate_call(src string, fmap map[string]fun_spec, node gexpr.INode) error {
	if node.Type() != gexpr.NodeTypeCall {
		return &errs.ErrPredicateBuilderFailed{
			Src:   src,
			Cause: errors.New("func expected"),
		}
	}
	f := fmap[node.Func()]
	inv_args := func() error {
		return &errs.ErrPredicateBuilderFailed{
			Src:   src,
			Cause: fmt.Errorf("%s expected %d args", node.Func(), len(f.args)),
		}
	}
	if len(f.args) != len(node.Args()) {
		return inv_args()
	}
	for i, arg := range node.Args() {
		if f.args[i] != self.get_val_type(arg) {
			return inv_args()
		}
	}
	return nil
}

func (self *compiler) get_val_type(arg gexpr.INode) gexpr.NodeType {
	if arg.Type() != gexpr.NodeTypeVar {
		return arg.Type()
	}
	switch arg.VarName() {
	case var_resp:
		return gexpr.NodeTypeInt
	case var_label:
		return gexpr.NodeTypeStr
	default:
		panic(fmt.Sprintf("unknown var: %s", arg.VarName()))
	}
}

func exec_op(c ctx, node gexpr.INode) bool {
	f := c.fmap[node.Func()]
	return f.f(c, node.Args())
}
