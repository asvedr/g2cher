package list_parsers

import (
	"proto"
	"strings"

	"gitlab.com/asvedr/giter"
)

type cmd struct {
	parsers map[string]proto.IPostParser
}

func New(parsers map[string]proto.IPostParser) proto.IUiCmd {
	return &cmd{parsers: parsers}
}

func (cmd) Name() string {
	return "/lprs"
}

func (cmd) Help() string {
	return ": list of parsers"
}

func (self cmd) Process(_ string) (string, error) {
	if len(self.parsers) == 0 {
		return "No parsers found", nil
	}
	msg := strings.Join(giter.MKeys(self.parsers), ",")
	return msg, nil
}
