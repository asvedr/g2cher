package add_res

import (
	ents "entities/common"
	"errors"
	"proto"
	"strings"
	"utils"
)

type cmd struct {
	res_manager proto.IResourceManager
}

func New(res_manager proto.IResourceManager) proto.IUiCmd {
	return cmd{res_manager: res_manager}
}

func (cmd) Name() string {
	return "/addres"
}

func (cmd) Help() string {
	return "<name> <parser> <interval> <path>: add resource"
}

func (self cmd) Process(msg string) (string, error) {
	split := strings.Split(msg, " ")
	if len(split) != 5 {
		return "", errors.New("invalid command")
	}
	_, found := self.res_manager.GetResource(split[1])
	if found {
		return "", errors.New("resource name already used")
	}
	interval, err := utils.ParseInterval(split[3])
	if err != nil {
		return "", err
	}
	err = self.res_manager.AddResource(ents.Resource{
		Name:          split[1],
		Parser:        split[2],
		Path:          split[4],
		NotifInterval: int(interval.Seconds()),
		Active:        true,
	})
	return "Done", err
}
