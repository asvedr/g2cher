package upd_res

import (
	ents "entities/common"
	"errors"
	"proto"
	"strings"
	"utils"
)

type cmd struct {
	res_manager proto.IResourceManager
}

func New(res_manager proto.IResourceManager) proto.IUiCmd {
	return &cmd{res_manager: res_manager}
}

func (cmd) Name() string {
	return "/updres"
}

func (cmd) Help() string {
	return "<name> [active=t/f] [interval=<float><s/m/h>]: update resource"
}

func (self cmd) Process(msg string) (string, error) {
	split := strings.Split(msg, " ")
	if len(split) < 2 {
		return "", errors.New("invalid command")
	}
	res, found := self.res_manager.GetResource(split[1])
	if !found {
		return "", errors.New("resource not found")
	}
	var err error
	if err = on_prefix(split[2:], &res, upd_active, "active="); err != nil {
		return "", err
	}
	if err = on_prefix(split[2:], &res, upd_interval, "interval="); err != nil {
		return "", err
	}
	err = self.res_manager.UpdResource(res)
	return "Done", err
}

func on_prefix(
	tokens []string,
	res *ents.Resource,
	action func(string, *ents.Resource) error,
	prefix string,
) error {
	for _, token := range tokens {
		if strings.HasPrefix(token, prefix) {
			val := strings.TrimPrefix(token, prefix)
			return action(val, res)
		}
	}
	return nil
}

func upd_active(token string, res *ents.Resource) error {
	switch token {
	case "t":
		res.Active = true
	case "f":
		res.Active = false
	default:
		return errors.New("active= expected t or f")
	}
	return nil
}

func upd_interval(token string, res *ents.Resource) error {
	interval, err := utils.ParseInterval(token)
	if err != nil {
		return err
	}
	res.NotifInterval = int(interval.Seconds())
	return nil
}
