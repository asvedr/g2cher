package list_res

import (
	ents "entities/common"
	"fmt"
	"proto"
	"strings"

	"gitlab.com/asvedr/giter"
)

type cmd struct {
	res_manager proto.IResourceManager
}

func New(res_manager proto.IResourceManager) proto.IUiCmd {
	return &cmd{res_manager: res_manager}
}

func (cmd) Name() string {
	return "/lres"
}

func (cmd) Help() string {
	return ": list of resources"
}

func (self cmd) Process(_ string) (string, error) {
	mf := func(r ents.Resource) string {
		return fmt.Sprintf("%s (path=%s)", r.Name, r.Path)
	}
	list := giter.Map(self.res_manager.GetAll(), mf)
	if len(list) == 0 {
		return "No resources found", nil
	} else {
		return strings.Join(list, "\n"), nil
	}
}
