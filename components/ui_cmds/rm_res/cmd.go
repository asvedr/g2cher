package rm_res

import (
	"errors"
	"proto"
	"strings"
)

type cmd struct {
	res_manager proto.IResourceManager
}

func New(res_manager proto.IResourceManager) proto.IUiCmd {
	return &cmd{res_manager: res_manager}
}

func (cmd) Name() string {
	return "/rmres"
}

func (cmd) Help() string {
	return "<name>: remove resource"
}

func (self cmd) Process(msg string) (string, error) {
	split := strings.Split(msg, " ")
	if len(split) != 2 {
		return "", errors.New("invalid command")
	}
	err := self.res_manager.RemoveResources([]string{split[1]})
	return "Done", err
}
