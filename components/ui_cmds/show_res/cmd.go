package show_res

import (
	"errors"
	"fmt"
	"proto"
	"strings"
)

type cmd struct {
	res_manager proto.IResourceManager
}

func New(res_manager proto.IResourceManager) proto.IUiCmd {
	return &cmd{res_manager: res_manager}
}

func (cmd) Name() string {
	return "/shres"
}

func (cmd) Help() string {
	return "<name>: show resource"
}

func (self cmd) Process(msg string) (string, error) {
	split := strings.Split(msg, " ")
	if len(split) != 2 {
		return "", errors.New("invalid command")
	}
	res, found := self.res_manager.GetResource(split[1])
	if !found {
		return "", errors.New("resource not found")
	}
	preds := self.res_manager.GetPredicates(res.Name)
	txt := fmt.Sprintf(
		"Name: %s\nParser: %s\nPath: %s\nActive: %v\nInterval: %v",
		res.Name,
		res.Parser,
		res.Path,
		res.Active,
		res.NotifInterval,
	)
	if len(preds) == 0 {
		txt += "\nNo predicates found"
	}
	for _, pred := range preds {
		txt = fmt.Sprintf(
			"%s\n- \"%s\"(%s): %s",
			txt,
			pred.Data().Name,
			pred.Data().Mode.String(),
			pred.Data().Data,
		)
	}
	return txt, nil
}
