package rm_pred

import (
	"errors"
	"proto"
	"strings"
)

type cmd struct {
	res_manager proto.IResourceManager
}

func New(res_manager proto.IResourceManager) proto.IUiCmd {
	return cmd{res_manager: res_manager}
}

func (cmd) Name() string {
	return "/rmpred"
}

func (cmd) Help() string {
	return "<res-name> <rule-name>: remove predicate"
}

func (self cmd) Process(msg string) (string, error) {
	split := strings.Split(msg, " ")
	if len(split) != 3 {
		return "", errors.New("invalid command")
	}
	err := self.res_manager.RemovePredicate(split[1], split[2])
	return "Done", err
}
