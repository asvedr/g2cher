package add_pred

import (
	"errors"
	"proto"
	"strings"
)

type cmd struct {
	res_manager proto.IResourceManager
}

func New(res_manager proto.IResourceManager) proto.IUiCmd {
	return cmd{res_manager: res_manager}
}

func (cmd) Name() string {
	return "/addpred"
}

func (cmd) Help() string {
	return "<res-name> <rule-name> <i/e> <rule>: add predicate"
}

func (self cmd) Process(msg string) (string, error) {
	split := strings.Split(msg, " ")
	if len(split) < 5 {
		return "", errors.New("invalid command")
	}
	var include bool
	switch split[3] {
	case "i":
		include = true
	case "e":
		include = false
	default:
		return "", errors.New("mode must be 'i' or 'e'")
	}
	err := self.res_manager.AddPredicate(
		split[1],
		split[2],
		strings.Join(split[4:], " "),
		include,
	)
	return "Done", err
}
