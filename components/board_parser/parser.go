package board_parser

import (
	ents "entities/common"
	errs "entities/errors"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/asvedr/giter"
	"golang.org/x/net/html"
)

type Parser struct{}

func (Parser) Name() string {
	return "2ch"
}

func (Parser) ParsePage(node *html.Node) ([]*ents.NewDbPost, error) {
	threads := find_all(
		node,
		func(node *html.Node) bool {
			return get_attr(node, "class") == "thread"
		},
	)
	if len(threads) == 0 {
		err := errs.ErrCanNotParsePage{Msg: "threads not found"}
		return nil, err
	}
	return giter.MapErr(threads, parse_thread)
}

func parse_thread(node *html.Node) (*ents.NewDbPost, error) {
	thread_id := get_attr(node, "id")
	if thread_id == "" {
		return nil, errs.ErrCanNotParsePage{Msg: "thread id not found"}
	}
	link, err := prepare_link(thread_id)
	if err != nil {
		return nil, err
	}
	responses, err := get_missed_answers_count(node)
	if err != nil {
		return nil, err
	}
	label_tag := find_one(
		node,
		func(child *html.Node) bool {
			return strings.HasPrefix(get_attr(child, "class"), "post__message")
		},
	)
	if label_tag == nil {
		return nil, errs.ErrCanNotParsePage{Msg: "Label tag not found"}
	}
	result := &ents.NewDbPost{
		Id:        thread_id,
		Link:      link,
		Label:     extract_text(label_tag, " "),
		Responses: responses,
	}
	return result, nil
}

func prepare_link(src string) (string, error) {
	if !strings.HasPrefix(src, "thread-") {
		return "", errs.ErrCanNotParsePage{Msg: "thread id is invalid"}
	}
	id := strings.TrimPrefix(src, "thread-")
	link := fmt.Sprintf("https://2ch.hk/b/res/%s.html", id)
	return link, nil
}

func get_missed_answers_count(node *html.Node) (int, error) {
	missed := find_one(
		node,
		func(child *html.Node) bool {
			return strings.HasPrefix(get_attr(child, "class"), "thread__missed")
		},
	)
	if missed == nil {
		return 0, nil
	}
	missed_text := extract_text(missed, " ")
	split := strings.Split(missed_text, " ")
	if len(split) < 2 {
		return -1, errs.ErrCanNotParsePage{Msg: "invalid missed threads data"}
	}
	count, err := strconv.Atoi(split[1])
	if err != nil {
		msg := fmt.Sprintf("invalid missed threads data: %v", err)
		return -1, &errs.ErrCanNotParsePage{Msg: msg}
	}
	return count, nil
}

func find_one(node *html.Node, predicate func(*html.Node) bool) *html.Node {
	if predicate(node) {
		return node
	}
	for child := node.FirstChild; child != nil; child = child.NextSibling {
		found := find_one(child, predicate)
		if found != nil {
			return found
		}
	}
	return nil
}

func find_all(node *html.Node, predicate func(*html.Node) bool) []*html.Node {
	var result struct{ data []*html.Node }
	var rec_down func(*html.Node)
	rec_down = func(node *html.Node) {
		if predicate(node) {
			result.data = append(result.data, node)
			return
		}
		for child := node.FirstChild; child != nil; child = child.NextSibling {
			rec_down(child)
		}
	}
	rec_down(node)
	return result.data
}

func get_attr(node *html.Node, name string) string {
	for _, attr := range node.Attr {
		if attr.Key == name {
			return attr.Val
		}
	}
	return ""
}

func extract_text(root *html.Node, sep string) string {
	var rec_down func(*html.Node) string
	rec_down = func(node *html.Node) string {
		if node.Type == html.TextNode {
			return node.Data
		}
		var tokens []string
		for ch := node.FirstChild; ch != nil; ch = ch.NextSibling {
			tokens = append(tokens, rec_down(ch))
		}
		return strings.Join(tokens, sep)
	}
	return strings.TrimSpace(rec_down(root))
}
