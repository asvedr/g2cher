package board_post_fetcher

import (
	ents "entities/common"
	errs "entities/errors"
	"net/http"
	"proto"

	"golang.org/x/net/html"
)

type Fetcher struct{}

func (self Fetcher) Fetch(url string, parser proto.IPostParser) ([]*ents.NewDbPost, error) {
	page, err := make_request(url)
	if err != nil {
		return nil, err
	}
	return parser.ParsePage(page)
}

func make_request(url string) (*html.Node, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, &errs.ErrRequestError{Cause: err}
	}
	if resp.StatusCode != 200 {
		return nil, &errs.ErrInvalidResp{Status: resp.StatusCode}
	}
	data, err := html.Parse(resp.Body)
	if err != nil {
		return nil, &errs.ErrCanNotParsePage{Msg: err.Error()}
	}
	return data, nil
}
