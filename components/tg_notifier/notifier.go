package tg_notifier

import (
	"fmt"
	"net/http"
	"proto"

	"github.com/PaulSonOfLars/gotgbot/v2"
	"gitlab.com/asvedr/cli_run"
)

type Notifier struct {
	bot     cli_run.Lazy[*gotgbot.Bot]
	chat_id int64
}

func New(
	token string,
	chat_id int64,
) proto.INotifier {
	return &Notifier{
		bot: cli_run.Lazy[*gotgbot.Bot]{
			Func: func() *gotgbot.Bot { return init_bot(token) },
		},
		chat_id: chat_id,
	}
}

func init_bot(token string) *gotgbot.Bot {
	bot, err := gotgbot.NewBot(token, &gotgbot.BotOpts{
		BotClient: &gotgbot.BaseBotClient{
			Client: http.Client{},
			DefaultRequestOpts: &gotgbot.RequestOpts{
				Timeout: gotgbot.DefaultTimeout, // Customise the default request timeout here
				APIURL:  gotgbot.DefaultAPIURL,  // As well as the Default API URL here (in case of using local bot API servers)
			},
		},
	})
	if err != nil {
		panic(fmt.Sprintf("failed to create new bot: %v", err))
	}
	return bot
}

func (self *Notifier) Notify(msg string) error {
	if self.chat_id == 0 {
		return nil
	}
	bot := self.bot.Force()
	_, err := bot.SendMessage(self.chat_id, msg, nil)
	return err
}
