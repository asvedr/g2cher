package app

import (
	"entrypoints/config_schema"
	"entrypoints/run"

	"gitlab.com/asvedr/cli_run"
)

func MakeRunEntrypoint() cli_run.IEntrypoint {
	return run.New(
		MakeFetcherProc(),
		MakeNotifierProc(),
		MakeCleanerProc(),
		MakeTgUiProc(),
	)
}

func MakeShowConfigSchema() cli_run.IEntrypoint {
	return config_schema.New()
}
