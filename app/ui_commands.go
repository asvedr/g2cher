package app

import (
	"components/ui_cmds/add_pred"
	"components/ui_cmds/add_res"
	"components/ui_cmds/list_parsers"
	"components/ui_cmds/list_res"
	"components/ui_cmds/rm_pred"
	"components/ui_cmds/rm_res"
	"components/ui_cmds/show_res"
	"components/ui_cmds/upd_res"
	"proto"
)

func CmdAddRes() proto.IUiCmd {
	return add_res.New(ResourceManager.Force())
}

func CmdListRes() proto.IUiCmd {
	return list_res.New(ResourceManager.Force())
}

func CmdRmRes() proto.IUiCmd {
	return rm_res.New(ResourceManager.Force())
}

func CmdShowRes() proto.IUiCmd {
	return show_res.New(ResourceManager.Force())
}

func CmdAddPred() proto.IUiCmd {
	return add_pred.New(ResourceManager.Force())
}

func CmdRmPred() proto.IUiCmd {
	return rm_pred.New(ResourceManager.Force())
}

func CmdListParsers() proto.IUiCmd {
	return list_parsers.New(Parsers)
}

func CmdUpdRes() proto.IUiCmd {
	return upd_res.New(ResourceManager.Force())
}
