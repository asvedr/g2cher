package app

import (
	"process/cleaner"
	"process/fetcher"
	"process/notifier"
	"process/tg_ui"
	"proto"
)

func MakeNotifierProc() proto.IProcess {
	c := Config.Force()
	return proto.StepProcToProc(notifier.New(
		TgNotifier.Force(),
		PostRepo.Force(),
		ResourceManager.Force(),
		c.NotifTimeout,
		c.MaxNotifAge,
	))
}

func MakeFetcherProc() proto.IProcess {
	c := Config.Force()
	return proto.StepProcToProc(fetcher.New(
		PostFetcher.Force(),
		Parsers,
		ResourceManager.Force(),
		PostRepo.Force(),
		c.ReqTimeout,
	))
}

func MakeCleanerProc() proto.IProcess {
	return proto.StepProcToProc(cleaner.New(
		PostRepo.Force(),
		Config.Force().MaxPostDbLifetime,
	))
}

func MakeTgUiProc() proto.IProcess {
	c := Config.Force()
	return tg_ui.New(
		[]proto.IUiCmd{
			CmdListRes(),
			CmdShowRes(),
			CmdAddRes(),
			CmdUpdRes(),
			CmdRmRes(),
			CmdAddPred(),
			CmdRmPred(),
			CmdListParsers(),
		},
		PostRepo.Force(),
		PredicateCompiler.Force(),
		c.TgToken,
		c.TgReceiver,
	)
}
