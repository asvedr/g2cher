package app

import (
	"cli_notifier"
	"components/board_parser"
	"components/board_post_fetcher"
	"components/predicate_compiler"
	"components/resource_manager"
	"components/tg_notifier"
	"config"
	"proto"

	"gitlab.com/asvedr/cli_run"
)

var Config = cli_run.Lazy[*config.Config]{
	Func: func() *config.Config {
		val, err := config.NewConfig()
		if err != nil {
			panic(err.Error())
		}
		return &val
	},
}

var Parsers = map[string]proto.IPostParser{"2ch": board_parser.Parser{}}

var PostFetcher = cli_run.Lazy[proto.IPostFetcher]{
	Func: func() proto.IPostFetcher {
		return board_post_fetcher.Fetcher{}
	},
}

var PredicateCompiler = cli_run.Lazy[proto.IPredicateCompiler]{
	Func: func() proto.IPredicateCompiler {
		return predicate_compiler.NewCompiler()
	},
}

var ResourceManager = cli_run.Lazy[proto.IResourceManager]{
	Func: func() proto.IResourceManager {
		man, err := resource_manager.New(
			ResourceRepo.Force(),
			PredicateRepo.Force(),
			PredicateCompiler.Force(),
		)
		if err != nil {
			panic(err.Error())
		}
		return man
	},
}

var CliNotifier = cli_run.Lazy[proto.INotifier]{
	Func: func() proto.INotifier {
		return &cli_notifier.Notifier{}
	},
}

var TgNotifier = cli_run.Lazy[proto.INotifier]{
	Func: func() proto.INotifier {
		c := Config.Force()
		return tg_notifier.New(c.TgToken, c.TgReceiver)
	},
}
