package app

import (
	"fmt"
	"proto"
	"sqlite"

	"gitlab.com/asvedr/cli_run"
	ss "gitlab.com/asvedr/safe_sqlite"
)

var db = cli_run.Lazy[ss.Db]{
	Func: func() ss.Db {
		db, err := ss.New(Config.Force().DbPath)
		if err != nil {
			panic(fmt.Sprintf("can not open db: %v", err))
		}
		return db
	},
}

var PostRepo = cli_run.Lazy[proto.IPostRepo]{
	Func: func() proto.IPostRepo {
		val, err := sqlite.NewPostRepo(db.Force())
		if err != nil {
			panic(err.Error())
		}
		return val
	},
}

var ResourceRepo = cli_run.Lazy[proto.IResourceRepo]{
	Func: func() proto.IResourceRepo {
		val, err := sqlite.NewResourceRepo(db.Force())
		if err != nil {
			panic(err.Error())
		}
		return val
	},
}

var PredicateRepo = cli_run.Lazy[proto.IPredicateRepo]{
	Func: func() proto.IPredicateRepo {
		val, err := sqlite.NewPredicateRepo(db.Force())
		if err != nil {
			panic(err.Error())
		}
		return val
	},
}

func InitDb() {
	ResourceRepo.Force()
	PredicateRepo.Force()
	PostRepo.Force()
}
