package entities

import (
	"fmt"
	"time"
)

type NewDbPost struct {
	Id           string
	Link         string
	Label        string
	Responses    int
	ResourceName string
}

type DbPost struct {
	NewDbPost
	Notified        time.Time
	LastSeenOnBoard time.Time
}

type Resource struct {
	Name   string
	Path   string
	Parser string
	Active bool
	// In secs
	NotifInterval int
}

type UpdResource struct {
	Name   string
	Path   *string
	Parser *string
	Active *bool
	// In secs
	NotifInterval *int
}

type PredicateMode int

const (
	PredicateModeInclude = 1
	PredicateModeExclude = 2
)

type NewPredicateData struct {
	Name         string
	Data         string
	Mode         PredicateMode
	ResourceName string
}

type PredicateData struct {
	NewPredicateData
	Id int
}

func (p *PredicateData) Downcast() NewPredicateData {
	return NewPredicateData{
		Name:         p.Name,
		Data:         p.Data,
		Mode:         p.Mode,
		ResourceName: p.ResourceName,
	}
}

func (m PredicateMode) String() string {
	switch m {
	case PredicateModeInclude:
		return "include"
	case PredicateModeExclude:
		return "exclude"
	default:
		panic(fmt.Sprintf("unknown mode: %d", m))
	}
}

func (self Resource) ToUpdate(other Resource) UpdResource {
	upd := UpdResource{Name: self.Name}
	if self.Path != other.Path {
		upd.Path = &other.Path
	}
	if self.Parser != other.Parser {
		upd.Parser = &other.Parser
	}
	if self.Active != other.Active {
		upd.Active = &other.Active
	}
	if self.NotifInterval != other.NotifInterval {
		upd.NotifInterval = &other.NotifInterval
	}
	return upd
}
