package errors

import "fmt"

type ErrRequestError struct{ Cause error }
type ErrInvalidResp struct {
	Status int
	Msg    string
}
type ErrCanNotParsePage struct {
	Msg string
}
type ErrPredicateBuilderFailed struct {
	Src   string
	Cause error
}
type ErrResourceNameAlreadyUsed struct{}
type ErrResourceNameNotFound struct{}
type ErrPredicateNameAlreadyUsed struct{}

func (e ErrRequestError) Error() string {
	return fmt.Sprintf("ErrRequestError: %v", e.Cause)
}

func (e ErrInvalidResp) Error() string {
	return fmt.Sprintf("ErrInvalidResp: (%v):%v", e.Status, e.Msg)
}

func (e ErrCanNotParsePage) Error() string {
	return fmt.Sprintf("CanNotParsePage: %v", e.Msg)
}

func (e ErrPredicateBuilderFailed) Error() string {
	return fmt.Sprintf(
		"Predicate builder failed (%v) on \"%s\"",
		e.Cause,
		e.Src,
	)
}

func (e ErrResourceNameAlreadyUsed) Error() string {
	return "Resource name already used"
}

func (e ErrResourceNameNotFound) Error() string {
	return "Resource name not found"
}

func (e ErrPredicateNameAlreadyUsed) Error() string {
	return "Predicate name already used"
}
