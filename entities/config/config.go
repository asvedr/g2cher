package config

import (
	"time"

	"gitlab.com/asvedr/cli_run"
)

type Config struct {
	DbPath            string
	ReqTimeout        time.Duration
	NotifTimeout      time.Duration
	MaxNotifAge       time.Duration
	MaxPostDbLifetime time.Duration
	TgToken           string
	TgReceiver        int64
}

func NewConfig() (Config, error) {
	builder := make_builder()
	parsed, err := builder.Parse()
	if err != nil {
		return Config{}, err
	}
	db_path := parsed.Named["DB"].(string)
	req_timeout := parsed.Named["REQ_TIMEOUT"].(int64)
	notif_timeout := parsed.Named["NOTIF_TIMEOUT"].(int64)
	max_notif_age := parsed.Named["MAX_NOTIF_AGE"].(int64)
	max_db_lifetime := parsed.Named["MAX_DB_LIFETIME"].(int64)
	tg_token := parsed.Named["TG_TOKEN"].(string)
	a_receiver, found := parsed.Named["TG_RECEIVER"]
	var receiver int64
	if found {
		receiver = a_receiver.(int64)
	}
	return Config{
		DbPath:            db_path,
		ReqTimeout:        time.Second * time.Duration(req_timeout),
		NotifTimeout:      time.Second * time.Duration(notif_timeout),
		MaxNotifAge:       time.Second * time.Duration(max_notif_age),
		MaxPostDbLifetime: time.Second * time.Duration(max_db_lifetime),
		TgToken:           tg_token,
		TgReceiver:        receiver,
	}, nil
}

func GetFields() []string {
	builder := make_builder()
	return builder.Params()
}

func make_builder() cli_run.IConfigBuilder {
	builder := cli_run.ConfigBuilder()
	builder.SetPrefix("G2CHER_")
	builder.AddDefaultParam(
		"DB",
		cli_run.Str{Default: "g2cher.db"},
		"path to db",
	)
	var zero int64 = 0
	builder.AddDefaultParam(
		"REQ_TIMEOUT",
		cli_run.Int{Gt: &zero, Default: 10},
		"Timeout between server requests(in seconds)",
	)
	builder.AddDefaultParam(
		"NOTIF_TIMEOUT",
		cli_run.Int{Gt: &zero, Default: 60 * 10},
		"Timeout between tg notifications (in seconds)",
	)
	builder.AddDefaultParam(
		"MAX_NOTIF_AGE",
		cli_run.Int{Gt: &zero, Default: 60 * 5},
		"Timeout between 2 notif for one post (in seconds)",
	)
	builder.AddDefaultParam(
		"MAX_DB_LIFETIME",
		cli_run.Int{Gt: &zero, Default: 60 * 60 * 24},
		"Timeout to remove post from DB if not seen on board",
	)
	builder.AddParam("TG_TOKEN", cli_run.Str{}, "Tg token")
	builder.AddOptParam(
		"TG_RECEIVER",
		cli_run.Int{},
		"Destination for notifications",
	)
	return builder
}
