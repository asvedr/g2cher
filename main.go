package main

import (
	"app"

	"gitlab.com/asvedr/cli_run"
)

var Version = "N/A"

func main() {
	app.InitDb()
	r := cli_run.NewRunner()
	r.SetUseCase(app.MakeRunEntrypoint())
	//r.SetDescription("Notification bot. Version: " + Version)
	//r.AddUseCase(app.MakeRunEntrypoint())
	//r.AddUseCase(app.MakeShowConfigSchema())
	r.Run()
}
