package utils

import (
	"errors"
	"strconv"
	"strings"
	"time"
)

func TrimStringToLen(src string, max_len int) string {
	var result string
	counter := 0
	for _, char := range src {
		if counter > max_len {
			break
		}
		result += string(char)
		counter += 1
	}
	return strings.TrimSpace(result)
}

func ParseInterval(src string) (time.Duration, error) {
	if len(src) == 0 {
		return 0, errors.New("invalid interval")
	}
	runes := []rune(src)
	var mult float64
	switch runes[len(runes)-1] {
	case 's':
		mult = float64(time.Second)
	case 'm':
		mult = float64(time.Minute)
	case 'h':
		mult = float64(time.Hour)
	default:
		return 0, errors.New("invalid interval suffix")
	}
	fl, err := strconv.ParseFloat(string(runes[:len(runes)-1]), 64)
	if err != nil {
		return 0, errors.New("invalid interval")
	}
	return time.Duration(mult * fl), nil
}
