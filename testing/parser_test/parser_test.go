package parser_test

import (
	"bytes"
	"components/board_parser"
	ents "entities/common"
	"os"
	"reflect"
	"testing"
	"utils"

	"golang.org/x/net/html"
)

func TestParsePage(t *testing.T) {
	bts, err := os.ReadFile("page.html")
	if err != nil {
		t.Fatal(err)
	}
	node, err := html.Parse(bytes.NewReader(bts))
	if err != nil {
		t.Fatal(err)
	}
	posts, err := board_parser.Parser{}.ParsePage(node)
	if err != nil {
		t.Fatal(err)
	}
	var to_check []ents.NewDbPost
	for _, post := range posts {
		val := ents.NewDbPost{
			Id:        post.Id,
			Link:      post.Link,
			Label:     utils.TrimStringToLen(post.Label, 10),
			Responses: post.Responses,
		}
		to_check = append(to_check, val)
	}
	expected := []ents.NewDbPost{
		{Id: "thread-266160769", Link: "https://2ch.hk/b/res/266160769.html", Label: "Главный нов", Responses: 0},
		{Id: "thread-294590810", Link: "https://2ch.hk/b/res/294590810.html", Label: "Сап, харкач", Responses: 5},
		{Id: "thread-294585283", Link: "https://2ch.hk/b/res/294585283.html", Label: "Сап Б  Наше", Responses: 56},
		{Id: "thread-294589801", Link: "https://2ch.hk/b/res/294589801.html", Label: "В старых", Responses: 4},
		{Id: "thread-294590987", Link: "https://2ch.hk/b/res/294590987.html", Label: "Ищу минус п", Responses: 0},
		{Id: "thread-294583616", Link: "https://2ch.hk/b/res/294583616.html", Label: "WEBM", Responses: 110},
		{Id: "thread-294589291", Link: "https://2ch.hk/b/res/294589291.html", Label: "День.   Мы", Responses: 72},
		{Id: "thread-294590164", Link: "https://2ch.hk/b/res/294590164.html", Label: "Сап аноны.", Responses: 18},
		{Id: "thread-294584156", Link: "https://2ch.hk/b/res/294584156.html", Label: "За последни", Responses: 151},
		{Id: "thread-294573713", Link: "https://2ch.hk/b/res/294573713.html", Label: "Кулинарный", Responses: 470},
		{Id: "thread-294586518", Link: "https://2ch.hk/b/res/294586518.html", Label: "Есть ли у л", Responses: 32},
		{Id: "thread-294588221", Link: "https://2ch.hk/b/res/294588221.html", Label: "Вот для одн", Responses: 19},
		{Id: "thread-294583934", Link: "https://2ch.hk/b/res/294583934.html", Label: "Ушел после", Responses: 224},
		{Id: "thread-294562646", Link: "https://2ch.hk/b/res/294562646.html", Label: "В чём секре", Responses: 72},
		{Id: "thread-294590616", Link: "https://2ch.hk/b/res/294590616.html", Label: "ЧЕРНЫХ ВАКА", Responses: 18},
		{Id: "thread-294455980", Link: "https://2ch.hk/b/res/294455980.html", Label: "ЛЕГЕНДАРНЫЙ", Responses: 447},
		{Id: "thread-294564479", Link: "https://2ch.hk/b/res/294564479.html", Label: "Рейт сборку", Responses: 208},
		{Id: "thread-294589905", Link: "https://2ch.hk/b/res/294589905.html", Label: "Тред фэнтез", Responses: 4},
		{Id: "thread-294556964", Link: "https://2ch.hk/b/res/294556964.html", Label: "Топ музыкал", Responses: 390},
		{Id: "thread-294582966", Link: "https://2ch.hk/b/res/294582966.html", Label: "ТРЕДШОТОВ Т", Responses: 64},
		{Id: "thread-294577570", Link: "https://2ch.hk/b/res/294577570.html", Label: "Я требую об", Responses: 336},
	}
	if !reflect.DeepEqual(to_check, expected) {
		t.Fatal(to_check)
	}
}
