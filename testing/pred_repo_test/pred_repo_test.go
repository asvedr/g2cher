package pred_repo_test

import (
	ents "entities/common"
	"proto"
	"reflect"
	"sqlite"
	"testing"

	"gitlab.com/asvedr/giter"
	ss "gitlab.com/asvedr/safe_sqlite"
)

func make_repo() proto.IPredicateRepo {
	base, err := ss.New(":memory:")
	if err != nil {
		panic(err.Error())
	}
	_, err = sqlite.NewResourceRepo(base)
	if err != nil {
		panic(err.Error())
	}
	repo, err := sqlite.NewPredicateRepo(base)
	if err != nil {
		panic(err.Error())
	}
	return repo
}

func TestGetEmpty(t *testing.T) {
	repo := make_repo()
	preds, err := repo.GetMap("a", "b", "c")
	if err != nil {
		t.Fatal(err)
	}
	if len(preds) != 0 {
		t.Fatal(preds)
	}

	preds, err = repo.GetMap()
	if err != nil {
		t.Fatal(err)
	}
	if len(preds) != 0 {
		t.Fatal(preds)
	}
}

func TestCreateGet(t *testing.T) {
	repo := make_repo()
	pred1 := ents.NewPredicateData{
		Name:         "abc",
		Data:         "resp>3",
		Mode:         ents.PredicateModeInclude,
		ResourceName: "a",
	}
	_, err := repo.Add(pred1)
	if err != nil {
		t.Fatal(err)
	}
	pred2 := ents.NewPredicateData{
		Name:         "def",
		Data:         `"111" in label`,
		Mode:         ents.PredicateModeExclude,
		ResourceName: "b",
	}
	_, err = repo.Add(pred2)
	if err != nil {
		t.Fatal(err)
	}

	preds, err := repo.GetMap("a", "b")
	if err != nil {
		t.Fatal(err)
	}
	as_new := giter.M2M(
		preds,
		func(k string, pd []*ents.PredicateData) (string, []ents.NewPredicateData) {
			f := func(p *ents.PredicateData) ents.NewPredicateData {
				return p.Downcast()
			}
			return k, giter.Map(pd, f)
		},
	)
	expected := map[string][]ents.NewPredicateData{
		"a": giter.V(pred1),
		"b": giter.V(pred2),
	}
	if !reflect.DeepEqual(as_new, expected) {
		t.Fatal(as_new)
	}

	err = repo.Remove(preds["a"][0].Id)
	if err != nil {
		t.Fatal(err)
	}

	preds, err = repo.GetMap("a", "b")
	if err != nil {
		t.Fatal()
	}
	as_new = giter.M2M(
		preds,
		func(k string, pd []*ents.PredicateData) (string, []ents.NewPredicateData) {
			f := func(p *ents.PredicateData) ents.NewPredicateData {
				return p.Downcast()
			}
			return k, giter.Map(pd, f)
		},
	)
	expected = map[string][]ents.NewPredicateData{"b": giter.V(pred2)}
	if !reflect.DeepEqual(as_new, expected) {
		t.Fatal(preds)
	}
}
