package post_repo_test

import (
	ents "entities/common"
	"proto"
	"reflect"
	"sqlite"
	"testing"
	"time"

	ss "gitlab.com/asvedr/safe_sqlite"
)

type setup struct {
	resources proto.IResourceRepo
	posts     proto.IPostRepo
}

func make_setup() setup {
	base, err := ss.New(":memory:")
	if err != nil {
		panic(err.Error())
	}
	resources, err := sqlite.NewResourceRepo(base)
	if err != nil {
		panic(err.Error())
	}
	posts, err := sqlite.NewPostRepo(base)
	if err != nil {
		panic(err.Error())
	}
	return setup{resources: resources, posts: posts}
}

func TestEmpty(t *testing.T) {
	repo := make_setup().posts
	rows, err := repo.GetPostsForNotify(time.Now())
	if err != nil || len(rows) > 1 {
		t.Fatalf("%v|%v", rows, err)
	}
}

func TestGetReadyForNotifSepByNotif(t *testing.T) {
	setup := make_setup()
	setup.resources.Add(ents.Resource{
		Name:          "a",
		NotifInterval: 1,
	})
	setup.resources.Add(ents.Resource{
		Name:          "b",
		NotifInterval: 5,
	})
	setup.posts.Register(
		&ents.NewDbPost{Id: "pa", ResourceName: "a"},
		&ents.NewDbPost{Id: "pb", ResourceName: "b"},
	)
	dur := -(time.Second * 2)
	setup.posts.SetNotified(time.Now().Add(dur), "pa", "pb")
	setup.posts.SetSeen(time.Now(), "pa", "pb")
	rows, err := setup.posts.GetPostsForNotify(time.Now().Add(dur))
	if err != nil {
		t.Fatal(err)
	}
	if len(rows) != 1 {
		t.Fatal(rows)
	}
	if rows[0].Id != "pa" {
		t.Fatal(*rows[0])
	}
}

func TestGetReadyForNotifSepBySeen(t *testing.T) {
	setup := make_setup()
	setup.resources.Add(ents.Resource{
		Name:          "a",
		NotifInterval: 1,
	})
	setup.posts.Register(
		&ents.NewDbPost{Id: "pa", ResourceName: "a"},
		&ents.NewDbPost{Id: "pb", ResourceName: "a"},
	)
	dur := -(time.Second * 2)
	setup.posts.SetSeen(time.Now().Add(dur), "pa", "pb")
	setup.posts.SetSeen(time.Now(), "pa")
	setup.posts.SetSeen(time.Now().Add(-time.Hour), "pb")
	rows, err := setup.posts.GetPostsForNotify(time.Now())
	if err != nil {
		t.Fatal(err)
	}
	if err != nil {
		t.Fatal(err)
	}
	if len(rows) != 1 {
		t.Fatal(rows)
	}
	if rows[0].Id != "pa" {
		t.Fatal(*rows[0])
	}
}

func TestAddGetRemove(t *testing.T) {
	setup := make_setup()
	setup.resources.Add(ents.Resource{
		Name:          "a",
		NotifInterval: 1,
	})
	new_post := ents.NewDbPost{
		Id:           "a",
		Link:         "http",
		Label:        "l",
		Responses:    10,
		ResourceName: "a",
	}
	err := setup.posts.Register(&new_post)
	if err != nil {
		t.Fatal(err)
	}
	posts, err := setup.posts.GetById("a")
	if err != nil {
		t.Fatal(err)
	}
	if len(posts) != 1 {
		t.Fatal(posts)
	}
	got := ents.NewDbPost{
		Id:           posts["a"].Id,
		Link:         posts["a"].Link,
		Label:        posts["a"].Label,
		Responses:    posts["a"].Responses,
		ResourceName: posts["a"].ResourceName,
	}
	if !reflect.DeepEqual(new_post, got) {
		t.Fatal(got)
	}

	err = setup.posts.Remove("a")
	if err != nil {
		t.Fatal(err)
	}

	posts, err = setup.posts.GetById("a")
	if err != nil || len(posts) > 0 {
		t.Fatalf("%v|%v", got, err)
	}
}

func TestNotif(t *testing.T) {
	setup := make_setup()
	new_post := ents.NewDbPost{
		Id:           "a",
		Link:         "http",
		Label:        "l",
		Responses:    10,
		ResourceName: "abc",
	}
	err := setup.posts.Register(&new_post)
	if err != nil {
		t.Fatal(err)
	}
	got, err := setup.posts.GetById("a")
	if err != nil {
		t.Fatal(err)
	}
	if got["a"].Notified.Unix() != 0 {
		t.Fatal(got["a"].Notified)
	}
	now := time.Now()
	if err := setup.posts.SetNotified(now, "a"); err != nil {
		t.Fatal(err)
	}
	got, err = setup.posts.GetById("a")
	if err != nil {
		t.Fatal(err)
	}
	if got["a"].Notified.Unix() != now.Unix() {
		t.Fatal(got["a"].Notified)
	}
}

func TestSeen(t *testing.T) {
	setup := make_setup()
	new_post := ents.NewDbPost{
		Id:           "a",
		Link:         "http",
		Label:        "l",
		Responses:    10,
		ResourceName: "abc",
	}
	err := setup.posts.Register(&new_post)
	if err != nil {
		t.Fatal(err)
	}
	got, err := setup.posts.GetById("a")
	if err != nil {
		t.Fatal(err)
	}
	diff := got["a"].LastSeenOnBoard.Unix() - time.Now().Unix()
	if diff > 1 {
		t.Fatal(got["a"].LastSeenOnBoard, " diff: ", diff)
	}
	seen := time.Now().Add(time.Second * time.Duration(10))
	if err := setup.posts.SetSeen(seen, "a"); err != nil {
		t.Fatal(err)
	}
	got, err = setup.posts.GetById("a")
	if err != nil {
		t.Fatal(err)
	}
	diff = got["a"].LastSeenOnBoard.Unix() - time.Now().Unix()
	if diff != 10 && diff != 9 {
		t.Fatal(got["a"].LastSeenOnBoard)
	}
}
