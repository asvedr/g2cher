package utils_test

import (
	"testing"
	"time"
	"utils"
)

func TestParseInterval(t *testing.T) {
	sources := map[string]time.Duration{
		"2s":   time.Second * 2,
		"3m":   time.Minute * 3,
		"4h":   time.Hour * 4,
		"0.5s": time.Second / 2,
	}
	for src, expected := range sources {
		val, err := utils.ParseInterval(src)
		if err != nil {
			t.Fatal(err)
		}
		if val != expected {
			t.Fatalf("%v|%v", val, expected)
		}
	}
}

func TestParseIntervalErr(t *testing.T) {
	_, err := utils.ParseInterval("abcs")
	if err == nil || err.Error() != "invalid interval" {
		t.Fatal(err)
	}
}
