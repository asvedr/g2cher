package notifier_proc_test

import (
	"components/predicate_compiler"
	ents "entities/common"
	"errors"
	"proto"
	"reflect"
	"testing"

	"gitlab.com/asvedr/giter"
)

func TestNotify(t *testing.T) {
	setup := make_setup()

	succ_post1 := ents.DbPost{}
	succ_post1.Id = "1"
	succ_post1.Link = "link1"
	succ_post1.Label = "label1"
	succ_post1.Responses = 0
	succ_post1.ResourceName = "res"

	fail_post := ents.DbPost{}
	fail_post.Id = "2"
	fail_post.Link = "link2"
	fail_post.Label = "label2"
	fail_post.Responses = 0
	fail_post.ResourceName = "res"

	succ_post2 := ents.DbPost{}
	succ_post2.Id = "3"
	succ_post2.Link = "link3"
	succ_post2.Label = "label3"
	succ_post2.Responses = 0
	succ_post2.ResourceName = "res"

	setup.post_repo.posts = []*ents.DbPost{&succ_post1, &fail_post, &succ_post2}
	setup.notifier.errs = map[string]error{
		"Post still active link2": errors.New("boo"),
	}
	cmp, err := predicate_compiler.NewCompiler().Compile("resp == 0")
	if err != nil {
		t.Fatal(err)
	}
	npd := ents.NewPredicateData{Mode: ents.PredicateModeInclude}
	cmp.SetData(&ents.PredicateData{NewPredicateData: npd})
	setup.res_manager.preds["res"] = []proto.IPredicate{cmp}

	err = setup.proc.RunIteration()

	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(
		setup.notifier.sent,
		giter.S(
			"Post still active link1",
			"Post still active link2",
			"Post still active link3",
		),
	) {
		t.Fatal(setup.notifier.sent)
	}

	notified := giter.V2S(giter.MKeys(setup.post_repo.notified))
	if !reflect.DeepEqual(notified, giter.S("1", "3")) {
		t.Fatal(notified)
	}
}
