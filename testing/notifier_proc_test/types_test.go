package notifier_proc_test

import (
	ents "entities/common"
	"process/notifier"
	"proto"
	"strings"
	"sync"
	"time"
)

type mock_notifier struct {
	mtx  sync.Mutex
	sent map[string]struct{}
	errs map[string]error
}

type post_repo struct {
	mtx      sync.Mutex
	notified map[string]time.Time
	posts    []*ents.DbPost
}

type res_manager struct {
	preds map[string][]proto.IPredicate
}

type setup struct {
	notifier    *mock_notifier
	post_repo   *post_repo
	res_manager *res_manager
	proc        proto.IStepProcess
}

func (self *mock_notifier) Notify(msg string) error {
	msg = strings.Replace(
		strings.Split(msg, "last_seen")[0],
		"\n",
		" ",
		-1,
	)
	msg = strings.TrimSpace(msg)
	self.mtx.Lock()
	defer self.mtx.Unlock()
	self.sent[msg] = struct{}{}
	val, found := self.errs[msg]
	if !found {
		return nil
	} else {
		return val
	}
}

func (self *post_repo) SetNotified(tm time.Time, ids ...string) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	for _, id := range ids {
		self.notified[id] = tm
	}
	return nil
}

func (self *post_repo) GetPostsForNotify(min_last_seen time.Time) ([]*ents.DbPost, error) {
	return self.posts, nil
}

func (*post_repo) Register(...*ents.NewDbPost) error                       { panic("") }
func (*post_repo) SetSeen(time.Time, ...string) error                      { panic("") }
func (*post_repo) GetById(...string) (map[string]*ents.DbPost, error)      { panic("") }
func (*post_repo) GetNotSeenForLong(threshold time.Time) ([]string, error) { panic("") }
func (*post_repo) Remove(...string) error                                  { panic("") }

func (self *res_manager) GetPredicates(res_name string) []proto.IPredicate {
	val, found := self.preds[res_name]
	if !found {
		return []proto.IPredicate{}
	}
	return val
}

func (*res_manager) GetAll() []ents.Resource                       { panic("") }
func (*res_manager) GetResource(name string) (ents.Resource, bool) { panic("") }
func (*res_manager) UpdResource(ents.Resource) error               { panic("") }
func (*res_manager) AddResource(ents.Resource) error               { panic("") }
func (*res_manager) RemoveResources(res_name []string) error       { panic("") }
func (*res_manager) AddPredicate(res_name string, pred_name string, rule string, include bool) error {
	panic("")
}
func (*res_manager) RemovePredicate(res_name string, pred_name string) error { panic("") }

func make_setup() setup {
	n := &mock_notifier{
		sent: make(map[string]struct{}),
		errs: make(map[string]error),
	}
	p := &post_repo{
		notified: make(map[string]time.Time),
	}
	r := &res_manager{
		preds: make(map[string][]proto.IPredicate),
	}
	s := time.Second
	return setup{
		notifier:    n,
		post_repo:   p,
		res_manager: r,
		proc:        notifier.New(n, p, r, s*2, s*5),
	}
}
