package fetcher_proc_test

import (
	ents "entities/common"
	"process/fetcher"
	"proto"
	"sync"
	"time"

	"gitlab.com/asvedr/giter"
	"golang.org/x/net/html"
)

type post_fetcher struct {
	mtx   sync.Mutex
	posts []*ents.NewDbPost
	calls []string
}
type parser struct{}
type res_manager struct {
	resources []ents.Resource
	preds     map[string][]proto.IPredicate
}
type post_repo struct {
	mtx        sync.Mutex
	registered []*ents.NewDbPost
	got        []ents.DbPost
	seen       []string
}

type setup struct {
	post_fetcher *post_fetcher
	res_manager  *res_manager
	post_repo    *post_repo
	proc         proto.IStepProcess
}

func (pf *post_fetcher) Fetch(url string, prsr proto.IPostParser) ([]*ents.NewDbPost, error) {
	pf.mtx.Lock()
	defer pf.mtx.Unlock()
	pf.calls = append(pf.calls, url)
	return pf.posts, nil
}

func (p parser) Name() string {
	panic("")
}

func (p parser) ParsePage(node *html.Node) ([]*ents.NewDbPost, error) {
	panic("")
}

func (self *res_manager) GetAll() []ents.Resource {
	return self.resources
}
func (self *res_manager) GetResource(name string) (ents.Resource, bool) {
	for _, res := range self.resources {
		if res.Name == name {
			return res, true
		}
	}
	return ents.Resource{}, false
}
func (self *res_manager) GetPredicates(res_name string) []proto.IPredicate {
	val, found := self.preds[res_name]
	if !found {
		return []proto.IPredicate{}
	} else {
		return val
	}
}
func (self res_manager) UpdResource(ents.Resource) error         { panic("") }
func (self res_manager) AddResource(ents.Resource) error         { panic("") }
func (self res_manager) RemoveResources(res_name []string) error { panic("") }
func (self res_manager) AddPredicate(res_name string, pred_name string, rule string, include bool) error {
	panic("")
}
func (self res_manager) RemovePredicate(res_name string, pred_name string) error { panic("") }

func (self *post_repo) Register(posts ...*ents.NewDbPost) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	self.registered = append(self.registered, posts...)
	return nil
}
func (self *post_repo) SetSeen(tm time.Time, ids ...string) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	self.seen = append(self.seen, ids...)
	return nil
}
func (self *post_repo) GetById(ids ...string) (map[string]*ents.DbPost, error) {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	m := giter.V2M(
		self.got,
		func(p ents.DbPost) (string, *ents.DbPost) { return p.Id, &p },
	)
	return m, nil
}
func (self *post_repo) GetPostsForNotify(min_last_seen time.Time) ([]*ents.DbPost, error) { panic("") }
func (self *post_repo) GetNotSeenForLong(threshold time.Time) ([]string, error)           { panic("") }
func (self *post_repo) SetNotified(time.Time, ...string) error                            { panic("") }
func (self *post_repo) Remove(...string) error                                            { panic("") }

func make_setup() setup {
	pf := &post_fetcher{}
	p := &parser{}
	rm := &res_manager{}
	pr := &post_repo{}
	return setup{
		post_fetcher: pf,
		res_manager:  rm,
		post_repo:    pr,
		proc: fetcher.New(
			pf,
			map[string]proto.IPostParser{"2ch": p},
			rm,
			pr,
			time.Second,
		),
	}
}
