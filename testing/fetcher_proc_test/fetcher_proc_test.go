package fetcher_proc_test

import (
	"components/predicate_compiler"
	ents "entities/common"
	"proto"
	"reflect"
	"testing"
)

func make_pred(src string) proto.IPredicate {
	val, err := predicate_compiler.NewCompiler().Compile(src)
	if err != nil {
		panic(err)
	}
	m := ents.PredicateData{}
	m.Mode = ents.PredicateModeInclude
	val.SetData(&m)
	return val
}

func TestFetchNone(t *testing.T) {
	setup := make_setup()
	err := setup.proc.RunIteration()
	if err != nil {
		t.Fatal(err)
	}
	if len(setup.post_repo.registered) != 0 {
		t.Fatal(setup.post_repo.registered)
	}
	if len(setup.post_fetcher.calls) != 0 {
		t.Fatal(setup.post_fetcher.calls)
	}
}

func TestFetchDisabled(t *testing.T) {
	setup := make_setup()
	setup.res_manager.resources = []ents.Resource{
		{Name: "2ch", Parser: "2ch", Active: false},
	}
	setup.res_manager.preds = map[string][]proto.IPredicate{
		"2ch": {make_pred("resp > 3")},
	}
	err := setup.proc.RunIteration()
	if err != nil {
		t.Fatal(err)
	}
	if len(setup.post_repo.registered) != 0 {
		t.Fatal(setup.post_repo.registered)
	}
	if len(setup.post_fetcher.calls) != 0 {
		t.Fatal(setup.post_fetcher.calls)
	}
}

func TestFetchNoPredicates(t *testing.T) {
	setup := make_setup()
	setup.res_manager.resources = []ents.Resource{
		{Name: "2ch", Parser: "2ch", Active: true},
	}
	setup.res_manager.preds = map[string][]proto.IPredicate{"2ch": {}}
	err := setup.proc.RunIteration()
	if err != nil {
		t.Fatal(err)
	}
	if len(setup.post_repo.registered) != 0 {
		t.Fatal(setup.post_repo.registered)
	}
	if len(setup.post_repo.seen) != 0 {
		t.Fatal(setup.post_repo.seen)
	}
	if len(setup.post_fetcher.calls) != 0 {
		t.Fatal(setup.post_fetcher.calls)
	}
}

func TestFetchOkSave(t *testing.T) {
	setup := make_setup()
	setup.res_manager.resources = []ents.Resource{
		{Name: "2ch", Parser: "2ch", Path: "2ch.com", Active: true},
	}
	setup.res_manager.preds = map[string][]proto.IPredicate{
		"2ch": {make_pred("resp > 3")},
	}
	setup.post_fetcher.posts = []*ents.NewDbPost{
		{Id: "a", Responses: 1},
		{Id: "b", Responses: 5},
	}
	err := setup.proc.RunIteration()
	if err != nil {
		t.Fatal(err)
	}
	if len(setup.post_repo.registered) != 1 {
		t.Fatal(setup.post_repo.registered)
	}
	if len(setup.post_repo.seen) != 0 {
		t.Fatal(setup.post_repo.seen)
	}
	if !reflect.DeepEqual(
		setup.post_repo.registered[0],
		&ents.NewDbPost{Id: "b", Responses: 5, ResourceName: "2ch"},
	) {
		t.Fatal(setup.post_repo.registered[0])
	}
	if !reflect.DeepEqual(setup.post_fetcher.calls, []string{"2ch.com"}) {
		t.Fatal(setup.post_fetcher.calls)
	}
}

func TestFetchOkSeen(t *testing.T) {
	setup := make_setup()
	setup.res_manager.resources = []ents.Resource{
		{Name: "2ch", Parser: "2ch", Path: "2ch.com", Active: true},
	}
	setup.res_manager.preds = map[string][]proto.IPredicate{
		"2ch": {make_pred("resp > 3")},
	}
	setup.post_fetcher.posts = []*ents.NewDbPost{
		{Id: "a", Responses: 1},
		{Id: "b", Responses: 5},
	}
	post := ents.DbPost{}
	post.Id = "b"
	setup.post_repo.got = []ents.DbPost{post}
	err := setup.proc.RunIteration()
	if err != nil {
		t.Fatal(err)
	}
	if len(setup.post_repo.registered) != 0 {
		t.Fatal(setup.post_repo.registered)
	}
	if !reflect.DeepEqual(setup.post_repo.seen, []string{"b"}) {
		t.Fatal(setup.post_repo.seen)
	}
	if !reflect.DeepEqual(setup.post_fetcher.calls, []string{"2ch.com"}) {
		t.Fatal(setup.post_fetcher.calls)
	}
}
