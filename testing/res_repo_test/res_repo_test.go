package res_repo_test

import (
	ents "entities/common"
	"proto"
	"reflect"
	"sqlite"
	"testing"

	"gitlab.com/asvedr/giter"
	ss "gitlab.com/asvedr/safe_sqlite"
)

func make_setup() proto.IResourceRepo {
	db, err := ss.New(":memory:")
	if err != nil {
		panic(err.Error())
	}
	repo, err := sqlite.NewResourceRepo(db)
	if err != nil {
		panic(err.Error())
	}
	return repo
}

func TestAddUpdateDelete(t *testing.T) {
	repo := make_setup()
	res := ents.Resource{
		Name:          "a",
		Path:          "a.com",
		Parser:        "2ch",
		Active:        true,
		NotifInterval: 3,
	}
	err := repo.Add(res)
	if err != nil {
		t.Fatal(err)

	}
	got, err := repo.GetAll()
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(got, giter.V(&res)) {
		t.Fatal(got)
	}
	changed := res
	changed.Active = false
	upd := res.ToUpdate(changed)
	err = repo.Update(upd)
	if err != nil {
		t.Fatal(err)
	}
	got, err = repo.GetAll()
	if !reflect.DeepEqual(got, giter.V(&changed)) {
		t.Fatal(got)
	}
	err = repo.Remove(res.Name)
	if err != nil {
		t.Fatal(err)
	}
	got, err = repo.GetAll()
	if err != nil || len(got) != 0 {
		t.Fatalf("%v|%v", got, err)
	}
}
