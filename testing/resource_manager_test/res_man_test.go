package resource_manager_test

import (
	"components/predicate_compiler"
	"components/resource_manager"
	e "entities/common"
	errs "entities/errors"
	"errors"
	"proto"
	"sqlite"
	"testing"

	ss "gitlab.com/asvedr/safe_sqlite"
)

type setup struct {
	res_repo  proto.IResourceRepo
	pred_repo proto.IPredicateRepo
	compiler  proto.IPredicateCompiler
}

func (self setup) manager() proto.IResourceManager {
	man, err := resource_manager.New(
		self.res_repo,
		self.pred_repo,
		self.compiler,
	)
	if err != nil {
		panic(err.Error())
	}
	return man
}

func make_setup() setup {
	db, err := ss.New(":memory:")
	if err != nil {
		panic(err.Error())
	}
	res_repo, err := sqlite.NewResourceRepo(db)
	if err != nil {
		panic(err.Error())
	}
	pred_repo, err := sqlite.NewPredicateRepo(db)
	if err != nil {
		panic(err.Error())
	}
	return setup{
		res_repo:  res_repo,
		pred_repo: pred_repo,
		compiler:  predicate_compiler.NewCompiler(),
	}
}

func TestEmpty(t *testing.T) {
	man := make_setup().manager()
	if len(man.GetAll()) != 0 {
		t.Fatal(man.GetAll())
	}
}

func TestRegisterResSetPreds(t *testing.T) {
	man := make_setup().manager()
	err := man.AddResource(e.Resource{
		Name:          "res_name",
		Path:          "res_path",
		Parser:        "res_parser",
		Active:        true,
		NotifInterval: 10,
	})
	if err != nil {
		t.Fatal(err)
	}
	res, found := man.GetResource("res_name")
	if !found {
		t.Fatal("no res")
	}
	err = man.AddPredicate(res.Name, "resp count", "resp > 3", true)
	if err != nil {
		t.Fatalf("%v|%v", res, err)
	}

	preds := man.GetPredicates(res.Name)
	if len(preds) != 1 {
		t.Fatal(preds)
	}
	good := &e.DbPost{NewDbPost: e.NewDbPost{Responses: 4}}
	bad := &e.DbPost{NewDbPost: e.NewDbPost{Responses: 2}}
	if !preds[0].Check(good) || preds[0].Check(bad) {
		t.Fatalf("%v|%v", preds[0].Check(good), preds[0].Check(bad))
	}
}

func TestRegisterResDumpLoad(t *testing.T) {
	setup := make_setup()
	setup.manager().AddResource(e.Resource{
		Name:          "name",
		NotifInterval: 1,
	})
	res, _ := setup.manager().GetResource("name")
	setup.manager().AddPredicate(res.Name, "resp count", "resp > 3", true)
	preds := setup.manager().GetPredicates(res.Name)
	if len(preds) != 1 {
		t.Fatal(preds)
	}
}

func TestDuplicateRes(t *testing.T) {
	man := make_setup().manager()
	err := man.AddResource(e.Resource{
		Name:          "n",
		NotifInterval: 1,
	})
	if err != nil {
		t.Fatal(err)
	}
	err = man.AddResource(e.Resource{
		Name:          "n",
		NotifInterval: 1,
	})
	if !errors.Is(err, &errs.ErrResourceNameAlreadyUsed{}) {
		t.Fatal(err)
	}
}

func TestDuplicatePred(t *testing.T) {
	man := make_setup().manager()
	err := man.AddResource(e.Resource{
		Name:          "r",
		NotifInterval: 1,
	})
	if err != nil {
		t.Fatal(err)
	}
	err = man.AddPredicate("r", "n", "resp > 0", true)
	if err != nil {
		t.Fatal(err)
	}
	err = man.AddPredicate("r", "n", "resp < 0", false)
	if !errors.Is(err, &errs.ErrPredicateNameAlreadyUsed{}) {
		t.Fatal(err)
	}
}

func TestInvalidPred(t *testing.T) {
	man := make_setup().manager()
	err := man.AddResource(e.Resource{
		Name:          "r",
		NotifInterval: 1,
	})
	if err != nil {
		t.Fatal(err)
	}
	err = man.AddPredicate("r", "n", "abc", true)
	if errors.Is(err, errs.ErrPredicateBuilderFailed{}) {
		t.Fatal(err)
	}
}
