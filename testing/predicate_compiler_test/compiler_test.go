package predicate_compiler_test

import (
	"components/predicate_compiler"
	ents "entities/common"
	errs "entities/errors"
	"testing"
	"time"
)

func TestInvalid(t *testing.T) {
	compiler := predicate_compiler.NewCompiler()
	_, err := compiler.Compile("abcdef")
	_, casted := err.(*errs.ErrPredicateBuilderFailed)
	if !casted {
		t.Fatal(err)
	}
}

func TestCompileGt(t *testing.T) {
	compiler := predicate_compiler.NewCompiler()
	f, err := compiler.Compile("resp>10")
	if err != nil {
		t.Fatal(err)
	}
	gt := &ents.DbPost{NewDbPost: ents.NewDbPost{Responses: 12}}
	lt := &ents.DbPost{NewDbPost: ents.NewDbPost{Responses: 9}}
	if !(f.Check(gt) && !f.Check(lt)) {
		t.Fatalf("%v|%v", f.Check(gt), f.Check(lt))
	}
}

func TestCompileLt(t *testing.T) {
	compiler := predicate_compiler.NewCompiler()
	f, err := compiler.Compile("resp<5")
	if err != nil {
		t.Fatal(err)
	}
	gt := &ents.DbPost{NewDbPost: ents.NewDbPost{Responses: 9}}
	lt := &ents.DbPost{NewDbPost: ents.NewDbPost{Responses: 4}}
	if !(!f.Check(gt) && f.Check(lt)) {
		t.Fatalf("%v|%v", f.Check(gt), f.Check(lt))
	}
}

func TestCompileHas(t *testing.T) {
	compiler := predicate_compiler.NewCompiler()
	f, err := compiler.Compile(`"beba" in label`)
	if err != nil {
		t.Fatal(err)
	}
	has := &ents.DbPost{NewDbPost: ents.NewDbPost{Label: "omg it's beba lol"}}
	hasnot := &ents.DbPost{NewDbPost: ents.NewDbPost{Label: ""}}
	if !(f.Check(has) && !f.Check(hasnot)) {
		t.Fatalf("%v|%v", f.Check(has), f.Check(hasnot))
	}
}

func TestCompileHasNot(t *testing.T) {
	compiler := predicate_compiler.NewCompiler()
	f, err := compiler.Compile(`!("boba" in label)`)
	if err != nil {
		t.Fatal(err)
	}
	has := &ents.DbPost{NewDbPost: ents.NewDbPost{Label: "omg it's boba lol"}}
	hasnot := &ents.DbPost{NewDbPost: ents.NewDbPost{Label: "only beba here"}}
	if !(!f.Check(has) && f.Check(hasnot)) {
		t.Fatalf("%v|%v", f.Check(has), f.Check(hasnot))
	}
}

func TestSeen(t *testing.T) {
	compiler := predicate_compiler.NewCompiler()
	f, err := compiler.Compile("notified()")
	if err != nil {
		t.Fatal(err)
	}
	has := &ents.DbPost{Notified: time.Now()}
	hasnot := &ents.DbPost{}
	if !(f.Check(has) && !f.Check(hasnot)) {
		t.Fatalf("%v|%v", f.Check(has), f.Check(hasnot))
	}
}
